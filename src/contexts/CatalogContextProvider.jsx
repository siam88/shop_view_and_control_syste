import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios';
import { ADMIN_URL } from '../Config';

export const CatalogContext = createContext();


const CatalogContextProvider = ({ children }) => {
    const [brands, setBrands] = useState();
    const [categories, setCategories] = useState();
    const [childCategories, setChildCategories] = useState();

    const [archivedBrands, setArchivedBrands] = useState();
    const [archivedCategories, setArchivedCategories] = useState();
    const [archivedChildCategories, setArchivedChildCategories] = useState();

    const [successMessage, setSuccessMessage] = useState();
    const [errorMessage, setErrorMessage] = useState();




    //brand
    const getBrands = async (archived) => {


        try {
            let changableUrl = `/brands`;
            if (archived) {
                changableUrl = `${changableUrl}/archived`
            }
            const res = await axios.get(changableUrl);
            setBrands(res.data);
            TimeoutStatus("success");
            return true;
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();
            console.log("Error getting brands ->", Error.response ? Error.response.message : Error.message)
            return false;
        }
    }

    const createBrand = async FormData => {
        console.log(FormData)
        try {
            const url = `/brands`;
            await axios.post(url, FormData).then(res => setSuccessMessage(res.data.message));
            TimeoutStatus("success");

        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();
            console.log("Error creating  brand ->", Error.response.data.message)
            return false;
        }
    }

    const updateBrand = async (FormData, id, archived) => {

        try {
            let changableUrl = `/brands`;
            if (archived) {
                changableUrl = `${changableUrl}/archived`;
            }
            await axios.put(`${changableUrl}/${id}`, FormData).then(res => setSuccessMessage(res.data.message))
            TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();
            const url = `/brands`;
            console.log("Error updating  brand ->", Error.response.data.message)
            return false;
        }
    }



    const deleteBrand = async (FormData, archived) => {
        let changableUrl = `/brands`;
        if (archived) {
            changableUrl = `${changableUrl}/archived`
        }
        try {
            console.log(FormData)
            await axios.delete(`${changableUrl}/${FormData.id}`, FormData).then(res => setSuccessMessage(res.data.message))
            TimeoutStatus("success");

        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error adding Archived Brand ->", Error.response.data.message)
            return false;
        }
    }




    //category
    const getCategories = async () => {
        try {
            const res = await axios.get(`/categories`);

            setCategories(res.data);
            TimeoutStatus("success");
            return true;
        } catch (Error) {
            TimeoutStatus();

            console.log("Error getting categories ->", Error.response ? Error.response.message : Error.message)
            return false;
        }
    }

    const createCategory = async FormData => {
        try {
            console.log(FormData)
            // await axios.post(`/categories`, FormData).then(res => setSuccessMessage(res.data.message));
            TimeoutStatus("success");
        } catch (Error) {
            TimeoutStatus();

            setErrorMessage(Error.response.data.message);
            console.log("Error creating  catagory ->", Error.response.data.message)
            return false;
        }

    }

    const updateCategory = async (FormData, id) => {
        try {
            await axios.put(`/categories/${id}`, FormData).then(res => setSuccessMessage(res.data.message))
            TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error updating  catagory ->", Error.response.data.message)
            return false;
        }
    }

    const getArchivedCategories = async () => {
        try {
            // const res = await axios.get(`/categories/archived`);

            // setArchivedCategories(res.data);
            // TimeoutStatus("success");
            // return true;
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error getting  archive brands ->", Error.response ? Error.response.message : Error.message)
            return false;
        }
    }

    const addArchivedCategory = async (FormData) => {
        try {
            //await axios.post(`/categories/archived`, FormData).then(res => setSuccessMessage(res.data.message));
            // TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error creating archived  catagory ->", Error.response.data.message)
            return false;
        }
    }

    const deleteArchivedCategory = async FormData => {
        try {
            // await axios.delete(`/categories/archived`, FormData).then(res => setSuccessMessage(res.data.message));
            // TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error deleting archived  catagory ->", Error.response.data.message)
            return false;
        }
    }

    const redoArchivedCategory = async FormData => {
        try {
            // await axios.post(`/categories/archived`, FormData).then(res => setSuccessMessage(res.data.message));
            // TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error deleting archived  catagory ->", Error.response.data.message)
            return false;
        }
    }
    //child catagory
    const createChildCategory = async FormData => {
        try {
            await axios.post(`/child_categories`, FormData).then(res => setSuccessMessage(res.data.message));
            TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error creating child catagory ->", Error.response.data.message)
            return false;
        }
    }

    const getChildCategories = async () => {
        try {
            const res = await axios.get(`/child_categories`);
            setChildCategories(res.data);
            TimeoutStatus("success");
            return true;
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error getting child catagory ->", Error.response ? Error.response.message : Error.message)
            return false;
        }
    }

    const updateChildCategory = async (FormData, id) => {
        try {
            await axios.put(`/child_categories/${id}`, FormData).then(res => setSuccessMessage(res.data.message))
            TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error updating child catagory ->", Error.response ? Error.response.message : Error.message)
            return false;
        }
    }

    const getArchivedChildategories = async () => {
        try {
            // const res = await axios.get(`/child_categories/archived`);

            // setArchivedChildCategories(res.data);
            // TimeoutStatus("success");
            // return true;
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error getting archived child catagory ->", Error.response ? Error.response.message : Error.message)
            return false;
        }
    }

    const addArchivedChildCategory = async (FormData) => {
        try {
            console.log(FormData)
            //await axios.post(`/child_categories/archived`, FormData).then(res => setSuccessMessage(res.data.message));
            // TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error creating archived  catagory ->", Error.response.data.message)
            return false;
        }
    }
    const deleteArchivedChildCategory = async FormData => {
        try {
            console.log(FormData);
            // await axios.delete(`/child_categories/archived`, FormData).then(res => setSuccessMessage(res.data.message));
            // TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error deleting archived  catagory ->", Error.response.data.message)
            return false;
        }
    }

    const redoArchivedChildCategory = async FormData => {
        try {
            console.log(FormData);
            // await axios.post(`/child_categories/archived`, FormData).then(res => setSuccessMessage(res.data.message));
            // TimeoutStatus("success");
        } catch (Error) {
            setErrorMessage(Error.response.data.message);
            TimeoutStatus();

            console.log("Error deleting archived  catagory ->", Error.response.data.message)
            return false;
        }
    }




    const resetStatus = () => {
        setSuccessMessage('');
        setErrorMessage('');
    }

    const TimeoutStatus = (success) => {
        setTimeout(() => {
            if (success) {
                setSuccessMessage('');
            } else {
                setErrorMessage('');
            }

        }, 4000);
    }

    const values = {
        categories,
        createCategory,
        getCategories,
        updateCategory,

        archivedCategories,
        getArchivedCategories,
        addArchivedCategory,
        deleteArchivedCategory,
        redoArchivedCategory,

        childCategories,
        createChildCategory,
        getChildCategories,
        updateChildCategory,

        archivedChildCategories,
        getArchivedChildategories,
        addArchivedChildCategory,
        deleteArchivedChildCategory,
        redoArchivedChildCategory,

        brands,
        getBrands,
        createBrand,
        updateBrand,
        deleteBrand,
        archivedBrands,



        successMessage,
        setSuccessMessage,
        errorMessage,
        setErrorMessage,

        resetStatus
    }


    return (
        <CatalogContext.Provider value={values}>
            {children}
        </CatalogContext.Provider>
    )
}

export default CatalogContextProvider