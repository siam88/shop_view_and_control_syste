import React from 'react';
import { Route, Switch, Redirect, useRouteMatch, useHistory } from 'react-router-dom';

import { ReturnPolicy, PrivacyPolicy, TermsAndConditions, ContactUs, TeamMemberAdd, TeamSection, AboutUs, ArchivedFaqItems, EditFaq, EditFaqCategory, ArchivedFaqCategory, AddFaqCategory, FaqCategory, AddFaq, Faq, EditBlogArticle, EditBlogCategory, BlogCategoryArchived, AddBlogCategory, BlogCategory, ArchivedBlogCategory, Blog, AddBlog } from '../views/pages/Pages';

//error
import Page404 from '../views/errorPages/page404';

const Pages = () => {
    const { path } = useRouteMatch();
    // const history = useHistory();
    // const pathName = history.location.pathname;

    return (
        <Switch>
            {/**Blogs*/}
            <Route exact path={`${path}/blog`} component={Blog} />
            <Route exact path={`${path}/blog/add`} component={AddBlog} />
            <Route exact path={`${path}/blog/edit`} component={EditBlogArticle} />
            <Route exact path={`${path}/blog/archived`} component={ArchivedBlogCategory} />
            <Route exact path={`${path}/blog/category`} component={BlogCategory} />
            <Route exact path={`${path}/blog/category/add`} component={AddBlogCategory} />
            <Route exact path={`${path}/blog/category/archived`} component={BlogCategoryArchived} />
            <Route exact path={`${path}/blog/category/edit`} component={EditBlogCategory} />
            {/**FAQs*/}
            <Route exact path={`${path}/faq`} component={Faq} />
            <Route exact path={`${path}/faq/add`} component={AddFaq} />
            <Route exact path={`${path}/faq/edit`} component={EditFaq} />
            <Route exact path={`${path}/faq/archived`} component={ArchivedFaqItems} />

            <Route exact path={`${path}/faq/category`} component={FaqCategory} />
            <Route exact path={`${path}/faq/category/add`} component={AddFaqCategory} />
            <Route exact path={`${path}/faq/category/archived`} component={ArchivedFaqCategory} />
            <Route exact path={`${path}/faq/category/edit`} component={EditFaqCategory} />

            {/**About us*/}
            <Route exact path={`${path}/about-us`} component={AboutUs} />
            <Route exact path={`${path}/about-us/team`} component={TeamSection} />
            <Route exact path={`${path}/about-us/team/add`} component={TeamMemberAdd} />
            {/*Contact us*/}
            <Route exact path={`${path}/contact-us`} component={ContactUs} />

            {/**
             * privacy policy
             */
            }
            <Route exact path={`${path}/privacy-policy`} component={PrivacyPolicy} />
            {/**
             * return policy
             */}
            <Route exact path={`${path}/return-policy`} component={ReturnPolicy} />

            {   /**
             * TermsAndConditions
             */}
            <Route exact path={`${path}/terms-and-conditions`} component={TermsAndConditions} />
            {/*for redirect not found*/}
            <Route path={`*`} component={Page404} />
        </Switch>
    )
}

export default Pages