import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers'

const EditProducts = ({ match }) => {

    const parentUrl = ParentUrl(match.path);

    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="alert alert-success alert-dismissible">
                        <button type="button" className="close" data-dismiss="alert"></button>
            Show message after succesful CRUD </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Editing product: here comes the product name</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-arrow-left"></i></Link>
                                <button type="button" className="btn btn-primary ml-2" data-toggle="modal" data-target="#quickCategoryModal"><i className="fas fa-align-justify"></i> Add quick category</button>
                                <button type="button" className="btn btn-dark ml-2" data-toggle="modal" data-target="#quickBrandModal"><i className="fas fa-award"></i>  Add quick brand</button>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Product information</h3>
                        </div>
                        <div className="card-body">
                            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li className="nav-item"> <a className="nav-link active" id="pills-general-tab" data-toggle="pill" href="#pills-general" role="tab" aria-controls="pills-general" aria-selected="true">General</a> </li>
                                <li className="nav-item"> <a className="nav-link" id="pills-specs-tab" data-toggle="pill" href="#pills-specs" role="tab" aria-controls="pills-specs" aria-selected="false">Specifications</a> </li>
                                <li className="nav-item"> <a className="nav-link" id="pills-discount-tab" data-toggle="pill" href="#pills-discount" role="tab" aria-controls="pills-discount" aria-selected="false">Discount</a> </li>
                                <li className="nav-item"> <a className="nav-link" id="pills-images-tab" data-toggle="pill" href="#pills-images" role="tab" aria-controls="pills-images" aria-selected="false">Images</a> </li>
                                <li className="nav-item"> <a className="nav-link" id="pills-video-tab" data-toggle="pill" href="#pills-video" role="tab" aria-controls="pills-ivideo" aria-selected="false">Video</a> </li>
                                <li className="nav-item"> <a className="nav-link" id="pills-options-tab" data-toggle="pill" href="#pills-options" role="tab" aria-controls="pills-options" aria-selected="false">Options</a> </li>
                            </ul>
                            <div className="tab-content p-4" id="pills-tabContent">
                                <div className="tab-pane fade show active" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">
                                    <form id="productGeneral">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Product SKU<span className="form-required">*</span></label>
                                                    <input type="text" className="form-control" placeholder="Enter product name" />
                                                </div>
                                            </div>
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Product name<span className="form-required">*</span></label>
                                                    <input type="text" className="form-control" placeholder="Enter product name" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Product short description<span className="form-required">*</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="Enter product short description"></textarea>
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Choose a product category <span className="form-required">*</span></label>
                                                    <select className="form-control custom-select">
                                                        <option value="brand1">Category 1</option>
                                                        <option value="brand2">Category 2</option>
                                                        <option value="brand3">Category 3</option>
                                                    </select>
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Choose a product child category <span className="form-required">*</span></label>
                                                    <select className="form-control custom-select">
                                                        <option value="brand1">Child Category 1</option>
                                                        <option value="brand2">Child Category 2</option>
                                                        <option value="brand3">Child Category 3</option>
                                                    </select>
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Choose a product brand <span className="form-required">*</span></label>
                                                    <select className="form-control custom-select">
                                                        <option value="brand1">Brand 1</option>
                                                        <option value="brand2">Brand 2</option>
                                                        <option value="brand3">Brand 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">SEO meta keywords</label>
                                                    <input type="text" className="form-control" placeholder="keywords, sperated, by, comma" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">SEO meta description</label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="Enter product meta description between 50–160 characters. "></textarea>
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Regular price <span className="form-required">*</span></label>
                                                    <input type="text" className="form-control" placeholder="enter product price" value="" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Total in stock<span className="form-required">*</span></label>
                                                    <input type="text" className="form-control" placeholder="(no idea?)" value="" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Choose a tax group <span className="form-required">*</span></label>
                                                    <select className="form-control custom-select">
                                                        <option value="brand1">Tax 1</option>
                                                        <option value="brand2">Taxt 2</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12 p-3">
                                                <div className="form-group">
                                                    <label className="form-label">Product long description<span className="form-required">*</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="This needs to be replaced by Summernote"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6">
                                                <div className="form-group">
                                                    <div className="form-label">Publish?</div>
                                                    <label className="custom-switch">
                                                        <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                        <span className="custom-switch-indicator"></span> </label>
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Add product</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-specs" role="tabpanel" aria-labelledby="pills-specs-tab">
                                    <form id="productSpecs">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-secondary btn-sm"><i className="fe fe-plus-square mr-2"></i>Add new row</button>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Specification name</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="Like weight, dimentions etc " />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Specification description</label>
                                                    <div className="input-group">
                                                        <input type="text" className="form-control" placeholder="Description" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Add specifications</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-discount" role="tabpanel" aria-labelledby="pills-discount-tab">
                                    <form id="productDiscount">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Discounted price</label>
                                                    <input type="text" className="form-control" />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">From (Date & Hour)</label>
                                                    <input type="text" name="field-name" className="form-control" data-mask="00/00/0000 00:00:00" data-mask-clearifnotmatch="true" placeholder="00/00/0000 00:00:00" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Till (Date & Hour)</label>
                                                    <input type="text" name="field-name" className="form-control" data-mask="00/00/0000 00:00:00" data-mask-clearifnotmatch="true" placeholder="00/00/0000 00:00:00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Add discount</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-images" role="tabpanel" aria-labelledby="pills-images-tab">
                                    <form id="productImages">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className="form-label">Current product images</label>
                                                    <div className="row gutters-sm">
                                                        <div className="col-sm-2">
                                                            <label className="imagecheck mb-4">
                                                                <input name="imagecheck" type="checkbox" value="1" className="imagecheck-input" />
                                                                <figure className="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                            </label>
                                                        </div>
                                                        <div className="col-sm-2">
                                                            <label className="imagecheck mb-4">
                                                                <input name="imagecheck" type="checkbox" value="2" className="imagecheck-input" checked />
                                                                <figure className="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                            </label>
                                                        </div>
                                                        <div className="col-sm-2">
                                                            <label className="imagecheck mb-4">
                                                                <input name="imagecheck" type="checkbox" value="3" className="imagecheck-input" />
                                                                <figure className="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                            </label>
                                                        </div>
                                                        <div className="col-sm-2">
                                                            <label className="imagecheck mb-4">
                                                                <input name="imagecheck" type="checkbox" value="4" className="imagecheck-input" checked />
                                                                <figure className="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                            </label>
                                                        </div>
                                                        <div className="col-sm-2">
                                                            <label className="imagecheck mb-4">
                                                                <input name="imagecheck" type="checkbox" value="5" className="imagecheck-input" />
                                                                <figure className="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                            </label>
                                                        </div>
                                                        <div className="col-sm-2">
                                                            <label className="imagecheck mb-4">
                                                                <input name="imagecheck" type="checkbox" value="6" className="imagecheck-input" />
                                                                <figure className="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4 mb-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-danger">Delete slected images</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <div className="form-label">Add new product images (multiple selections are allowed)</div>
                                                    <div className="custom-file">
                                                        <input type="file" className="custom-file-input" name="example-file-input-custom" />
                                                        <label className="custom-file-label">Choose images</label>
                                                    </div>
                                                </div>
                        (image previews after upload)
                        <div className="row row-cards row-deck mt-4 mb-4">
                                                    <div className="col-sm-6 col-xl-3 p-2"> <img className="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
                                                    <div className="col-sm-6 col-xl-3 p-2"> <img className="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
                                                    <div className="col-sm-6 col-xl-3 p-2"> <img className="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
                                                    <div className="col-sm-6 col-xl-3 p-2"> <img className="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Add images</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab">
                                    <form id="productVideo">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className="form-label">YouTube or Vimeo Link</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="Link" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Add video</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-options" role="tabpanel" aria-labelledby="pills-options-tab">
                                    <form id="productOptions">
                                        <div className="row mb-4">
                                            <div className="col-md-12 mb-4">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-secondary btn-sm"><i className="fe fe-plus-square mr-2"></i>Add new option set</button>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Option set</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="For example colors" />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="row gutters-xs">
                                                    <div className="col-md-7">
                                                        <div className="form-group">
                                                            <label className="form-label">Option 1</label>
                                                            <input type="text" className="form-control" name="example-text-input" placeholder="Blue" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="form-group">
                                                            <label className="form-label">Price</label>
                                                            <input type="text" className="form-control" name="example-text-input" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-2">
                                                        <div className="form-group">
                                                            <label className="form-label">Add row</label>
                                                            <button type="button" className="btn btn-gray-dark btn-sm"><i className="fe fe-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row gutters-xs">
                                                    <div className="col-md-7">
                                                        <div className="form-group">
                                                            <label className="form-label">Option 1</label>
                                                            <input type="text" className="form-control" name="example-text-input" placeholder="Blue" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="form-group">
                                                            <label className="form-label">Price</label>
                                                            <input type="text" className="form-control" name="example-text-input" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-2">
                                                        <div className="form-group">
                                                            <label className="form-label">Delete row</label>
                                                            <button type="button" className="btn btn-danger btn-sm"><i className="fe fe-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row gutters-xs">
                                                    <div className="col-md-7">
                                                        <div className="form-group">
                                                            <label className="form-label">Option 1</label>
                                                            <input type="text" className="form-control" name="example-text-input" placeholder="Blue" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="form-group">
                                                            <label className="form-label">Price</label>
                                                            <input type="text" className="form-control" name="example-text-input" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-2">
                                                        <div className="form-group">
                                                            <label className="form-label">Add row</label>
                                                            <button type="button" className="btn btn-gray-dark btn-sm"><i className="fe fe-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Add options</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>














            {/**
         * quickCategoryModal
         */}
            <div className="modal fade" id="quickCategoryModal" tabindex="-1" role="dialog" aria-labelledby="quickCategoryModal" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="quickCategoryModalLabel">Add category</h5>
                        </div>
                        <div className="modal-body">
                            <form id="addQuickCategory">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label className="form-label">Category name<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter category name" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Category short description<span className="form-required">*</span></label>
                                            <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="Enter category short description"></textarea>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Add category image</div>
                                            <div className="custom-file">
                                                <input type="file" className="custom-file-input" name="example-file-input-custom" />
                                                <label className="custom-file-label">Choose image</label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Publish now?</div>
                                            <label className="custom-switch">
                                                <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                <span className="custom-switch-indicator"></span> </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

            { /**
             * quickBrandModal
             */}
            <div className="modal fade" id="quickBrandModal" tabindex="-1" role="dialog" aria-labelledby="quickBrandModal" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="quickBrandModalLabel">Add brand</h5>
                        </div>
                        <div className="modal-body">
                            <form id="addQuickBrand">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label className="form-label">Brand name<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter brand name" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Brand short description<span className="form-required">*</span></label>
                                            <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="Enter brand short description"></textarea>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Add brand image</div>
                                            <div className="custom-file">
                                                <input type="file" className="custom-file-input" name="example-file-input-custom" />
                                                <label className="custom-file-label">Choose image</label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Publish now?</div>
                                            <label className="custom-switch">
                                                <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                <span className="custom-switch-indicator"></span> </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default EditProducts