import React, { Fragment } from 'react';
import { useRouteMatch, Link } from 'react-router-dom';


const List = ({categories}) => {
    const { path } = useRouteMatch();
    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>Total Articles</th>
                        <th>Added date</th>
                        <th>Published</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {categories.map(category => (
                        <tr key={category.id}>
                            <td>{category.name}</td>
                            <td>126</td>
                            <td>	01-06-2019</td>
                            <td>
                                <span className="badge badge-success">Published</span>
                            </td>
                            <td>
                                <Link to={`${path}/edit`} className="btn btn-sm "><i className="fas fa-edit"></i></Link>
                                <Link to="" className="btn btn-sm "><i className="fas fa-archive"></i></Link>
                                <Link to="" className="btn btn-sm "><i className="fas fa-eye-slash"></i></Link>

                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </Fragment>
    )
}

export default List