import React from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';

const AddCoupons = ({ match }) => {
    const parentUrl = ParentUrl(match.path);


    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="alert alert-primary">
                        Only succesful transactions will be registered.
                            </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Editing coupon: here comes coupon name
</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-tags" /> View All Coupons</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Coupon Information</h3>
                        </div>
                        <div className="card-body">
                            <form id="addCategory">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Coupon name<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter coupon name" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Coupon code<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter coupon code" />
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Discount types</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option1" checked />
                                                    <span className="custom-control-label">Fixed</span> </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option2" />
                                                    <span className="custom-control-label">Percent</span> </label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Value/amount<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter value" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Start date</label>
                                            <div className="row gutters-xs">
                                                <div className="col-3">
                                                    <select name="user[day]" className="form-control custom-select">
                                                        <option value="">Day</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                        <option value="16">16</option>
                                                        <option value="17">17</option>
                                                        <option value="18">18</option>
                                                        <option value="19">19</option>
                                                        <option selected="selected" value="20">20</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>
                                                        <option value="30">30</option>
                                                        <option value="31">31</option>
                                                    </select>
                                                </div>
                                                <div className="col-5">
                                                    <select name="user[month]" className="form-control custom-select">
                                                        <option value="">Month</option>
                                                        <option value="1">January</option>
                                                        <option value="2">February</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option selected="selected" value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">August</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                </div>
                                                <div className="col-4">
                                                    <select name="user[year]" className="form-control custom-select">
                                                        <option value="">Year</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2013">2020</option>
                                                        <option value="2012">2021</option>
                                                        <option value="2011">2022</option>
                                                        <option value="2011">2023</option>
                                                        <option value="2011">2024</option>
                                                        <option value="2011">2025</option>
                                                        <option value="2011">2026</option>
                                                        <option value="2011">2027</option>
                                                        <option value="2011">2028</option>
                                                        <option value="2011">2029</option>
                                                        <option value="2011">2030</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">End date</label>
                                            <div className="row gutters-xs">
                                                <div className="col-3">
                                                    <select name="user[day]" className="form-control custom-select">
                                                        <option value="">Day</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                        <option value="16">16</option>
                                                        <option value="17">17</option>
                                                        <option value="18">18</option>
                                                        <option value="19">19</option>
                                                        <option selected="selected" value="20">20</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>
                                                        <option value="30">30</option>
                                                        <option value="31">31</option>
                                                    </select>
                                                </div>
                                                <div className="col-5">
                                                    <select name="user[month]" className="form-control custom-select">
                                                        <option value="">Month</option>
                                                        <option value="1">January</option>
                                                        <option value="2">February</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option selected="selected" value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">August</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                </div>
                                                <div className="col-4">
                                                    <select name="user[year]" className="form-control custom-select">
                                                        <option value="">Year</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2013">2020</option>
                                                        <option value="2012">2021</option>
                                                        <option value="2011">2022</option>
                                                        <option value="2011">2023</option>
                                                        <option value="2011">2024</option>
                                                        <option value="2011">2025</option>
                                                        <option value="2011">2026</option>
                                                        <option value="2011">2027</option>
                                                        <option value="2011">2028</option>
                                                        <option value="2011">2029</option>
                                                        <option value="2011">2030</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Usage limit per coupon</label>
                                            <input type="text" className="form-control" placeholder="Enter amount" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Usage limit per customer</label>
                                            <input type="text" className="form-control" placeholder="Enter amount" />
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Applies to category</div>
                                            <div>
                                                <label className="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" className="custom-control-input" name="example-inline-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Option 1</span> </label>
                                                <label className="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" className="custom-control-input" name="example-inline-checkbox2" value="option2" />
                                                    <span className="custom-control-label">Option 2</span> </label>
                                                <label className="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" className="custom-control-input" name="example-inline-checkbox3" value="option3" />
                                                    <span className="custom-control-label">Option 3</span> </label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Applies to one product</label>
                                            <select name="beast" id="select-product" className="form-control custom-select">
                                                <option value="1">Nice product</option>
                                                <option value="4">Good product</option>
                                                <option value="3">Product that needs to be sold</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-4">

                                    <div className="col-6">

                                        <div className="form-group">
                                            <div className="form-label">Publish now?</div>
                                            <label className="custom-switch">
                                                <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                <span className="custom-switch-indicator"></span> </label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="text-right">
                                            <button type="submit" className="btn btn-md btn-primary">Add coupon</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default AddCoupons