import React, { Fragment } from 'react';
import { useRouteMatch, Link } from 'react-router-dom';

const Table = () => {
    const { path } = useRouteMatch();
    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>Total questions answered</th>
                        <th>Added date</th>
                        <th>Published</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Category name goes here</td>
                        <td>126</td>
                        <td>01-06-2019</td>
                        <td>
                            <span className="badge badge-success">Active</span>
                        </td>
                        <td>
                            <Link to={`${path}/edit`} className="btn btn-sm "><i className="fas fa-edit"></i></Link>
                            <Link to="" className="btn btn-sm "><i className="fas fa-trash-alt"></i></Link>
                            <Link to="" className="btn btn-sm "><i class="fas fa-undo-alt"></i></Link>

                        </td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default Table