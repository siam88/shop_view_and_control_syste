

import React, { useContext, Fragment, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';
import LoadingSuspense from '../../../components/LoadingSuspense';

const List = () => {
    const catelogContext = useContext(CatalogContext)
    const { path } = useRouteMatch();


    async function getCategories() {
        const status = await catelogContext.getArchivedCategories();
        if (!status) console.log("error getting datas");
    }


    const handleDelete = (category) => {
        catelogContext.deleteArchivedCategory(category);

    }

    const handleRedo = (category) => {
        catelogContext.redoArchivedCategory(category)
    }
    useEffect(() => {
        getCategories();
    }, [])

    return (
        <Fragment>

            <div className="table-responsive">
                <table className="table">
                    <thead>
                        <tr>
                            <th>IMG</th>
                            <th>CATEGORY NAME</th>
                            <th>CATEGORY DESC</th>
                            <th>PRODUCT COUNT</th>
                            <th>Published</th>

                            <th>ACTIONS</th>

                        </tr>
                    </thead>
                    <tbody>
                        {catelogContext.archivedCategories?.map((e, i) =>
                            <tr key={i}>
                                <td><span className="avatar avatar-lg" style={{ backgroundImage: 'url(/assets/images/xyz.jpg)' }} /></td>
                                <td>{e.name}</td>
                                <td>{e.description}</td>
                                <td>100</td>
                                <td>
                                    {
                                        e.status ? <span className="badge badge-success">published</span> : <span className="badge badge-danger">unpublished</span>
                                    }
                                </td>
                                <td>
                                    <Link to={{
                                        pathname: `${path}/edit`,
                                        state: { category: e }
                                    }} className="btn btn-md "><i className="far fa-edit" /></Link>
                                    <Link to="#" className="btn btn-md " onClick={() => handleDelete(e)} ><i className="far fa-trash-alt" /></Link>
                                    <Link to="#" className="btn btn-md " onClick={() => handleRedo(e)} ><i className="fas fa-redo" /></Link>
                                </td>
                            </tr>


                        )}

                    </tbody>
                </table>
            </div>
        </Fragment>
    )
}

export default List









