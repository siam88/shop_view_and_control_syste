import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';
import List from './List';

const ShippingOptions = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="alert alert-success alert-dismissible">
                        <button type="button" className="close" data-dismiss="alert" />Show message after succesful CRUD </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Shipping settings</h3>
                            <div className="card-options">
                                <button className="btn btn-primary ml-2" type="button" data-toggle="modal" data-target="#addShippingModal" ><i className="fas fa-plus mr-2"></i>Add new shipping</button>

                                <Link to={`${parentUrl}/store-information`} className="btn btn-outline-dark ml-2"><i className="fas fa-info mr-2"></i>Store information</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Shipping information</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>
                </div>
            </div>




            {/**
             * addShippingModal
             */}
            <div className="modal fade" id="addShippingModal" tabindex="-1" role="dialog" aria-labelledby="addShippingModal" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="addShippingModalLabel">Add new Shipping</h5>
                        </div>
                        <div className="modal-body">
                            <form id="addShipping">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label className="form-label">Shipping name<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter shipping name" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Shipping cost<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter a value" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    )
}

export default ShippingOptions