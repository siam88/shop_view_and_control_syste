import React from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from "../../../components/CommonHelpers"

const EditOrder = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    console.log(parentUrl)
    return (
        <>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Edit Order Status</h3>
                            <div className="card-options">
                                <button type="button" className="btn btn-gray-dark ml-2" data-toggle="modal" data-target="#editOrderModal"><i className="fas fa-clock ml-2"></i> Change status</button>
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-dollar-sign mr-2"></i>View all orders</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">#ODR0015:<span className="tag tag-orange ml-2">pending</span>
                            </h3>
                        </div>
                        <div className="card-body">
                            <div className="row my-6">
                                <div className="col-6">
                                    <h5>Test shop</h5>
                                    <ul className="list-unstyled">
                                        <li><span className="text-muted">Address:&nbsp; </span>44 Shirley Ave. West Chicago, IL 60185, USA</li>
                                        <li><span className="text-muted">VAT nr:&nbsp; </span>123456789</li>
                                        <li><span className="text-muted">Chamber of commerce nr:&nbsp; </span>123456789</li>
                                    </ul>
                                </div>
                                <div className="col-6 text-right">
                                    <h5>Customer Name</h5>
                                    <ul className="list-unstyled">
                                        <li>44 Shirley Ave. West Chicago</li>
                                        <li>West Chicago, IL 60185</li>
                                        <li> USA</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="table-responsive push">
                                <table className="table table-bordered table-hover">
                                    <tr>
                                        <th className="text-center" style={{ width: "1%" }}></th>
                                        <th>Product</th>
                                        <th className="text-center" style={{ width: "1%" }}>Qnt</th>
                                        <th className="text-right" style={{ width: "1%" }}>Unit</th>
                                        <th className="text-right" style={{ width: "1%" }}>Amount</th>
                                    </tr>
                                    <tr>
                                        <td className="text-center">1</td>
                                        <td>
                                            <p className="font-w600 mb-1">Logo Creation</p>
                                            <div className="text-muted">Logo and business cards design</div>
                                        </td>
                                        <td className="text-center"> 1 </td>
                                        <td className="text-right">$1.800,00</td>
                                        <td className="text-right">$1.800,00</td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">2</td>
                                        <td>
                                            <p className="font-w600 mb-1">Online Store Design &amp; Development</p>
                                            <div className="text-muted">Design/Development for all popular modern browsers</div>
                                        </td>
                                        <td className="text-center"> 1 </td>
                                        <td className="text-right">$20.000,00</td>
                                        <td className="text-right">$20.000,00</td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">3</td>
                                        <td>
                                            <p className="font-w600 mb-1">App Design</p>
                                            <div className="text-muted">Promotional mobile application</div>
                                        </td>
                                        <td className="text-center"> 1 </td>
                                        <td className="text-right">$3.200,00</td>
                                        <td className="text-right">$3.200,00</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" className="font-w600 text-right">Subtotal</td>
                                        <td className="text-right">$25.000,00</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" className="font-w600 text-right">Vat Rate</td>
                                        <td className="text-right">20%</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" className="font-w600 text-right">Vat Due</td>
                                        <td className="text-right">$5.000,00</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" className="font-weight-bold text-uppercase text-right">Total Due</td>
                                        <td className="font-weight-bold text-right">$30.000,00</td>
                                    </tr>
                                </table>
                            </div>
                            <p className="text-muted text-center">Thank you very much for doing business with us. We look forward to working with you again!</p>


                        </div>
                    </div>

                    <div className="modal fade" id="editOrderModal" tabindex="-1" role="dialog" aria-labelledby="editOrderModal" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="editOrderModalLabel">Edit order status and send message to customer</h5>
                                </div>
                                <div className="modal-body">
                                    <form id="changeOrderStatus">
                                        <div className="form-group">
                                            <div className="form-label">Change order stauts to:</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option1" checked />
                                                    <span className="custom-control-label">Pending</span> </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option2" />
                                                    <span className="custom-control-label">Ready for shipment</span> </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option3" />
                                                    <span className="custom-control-label">Shipped</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group mt-6">
                                            <div className="form-label">Shipping methodes:</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option1" />
                                                    <span className="custom-control-label">Standard</span> </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option2" />
                                                    <span className="custom-control-label">UPS</span> </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option3" />
                                                    <span className="custom-control-label">DHL</span> </label>
                                            </div>
                                            <hr />
                                        </div>
                                        <div className="form-group mt-6">
                                            <label className="form-label">Track and trace if shipped</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="Track and trace nr" />
                                        </div>
                                    </form>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" className="btn btn-primary">Save changes and send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default EditOrder