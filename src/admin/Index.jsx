import React, { Fragment, useContext, Suspense } from 'react'
import { Route, useRouteMatch, Redirect, Switch, useHistory } from 'react-router-dom';
import { AuthContext } from '../contexts/AuthContextProvider';
import LoadingSuspense from './components/LoadingSuspense';
import Navbar from './layouts/Navbar';
import Footer from './layouts/Footer';
import Page404 from './views/errorPages/page404';


const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const CatalogAdminRoutes = React.lazy(() => import('./routes/Catalog'));
const SaleAdminRoutes = React.lazy(() => import('./routes/Sales'));
const PagesAdminRoutes = React.lazy(() => import('./routes/Pages'));
const ControlCenterAdminRoutes = React.lazy(() => import('./routes/ControlCenter'));
const Communication = React.lazy(() => import('./routes/Communication'));


let cssLoaded = false;


const Main = (props) => {
	const { path } = useRouteMatch();
	const history = useHistory();
	const pathName = history.location.pathname;

	if (cssLoaded === false) {
		cssLoaded = true &&
			import('./assets/css/backend.css');
	}

	const authContext = useContext(AuthContext);

	// if (!authContext.login) {
	// // 	return <Redirect to="/shop-admin/login" />
	// }
	// console.log(authContext.isLogin);
	// console.log(authContext.login);

	return (
		<Fragment>

			<div className="page">
				<div className="flex-fill">
					<Suspense fallback={<LoadingSuspense />}>

						<Navbar currentPath={pathName} path={path} />
						<main role="main" className="container py-4">

							<Switch>
								<Route exact path={`${path}`} component={Dashboard} />

								{/*Routes for Catalog menu*/}
								<Route path={`${path}/catalog`} component={CatalogAdminRoutes} />

								{/*Routes for Sales menu*/}
								<Route path={`${path}/sales`} component={SaleAdminRoutes} />

								{/*Routes for Pages menu*/}
								<Route path={`${path}/pages`} component={PagesAdminRoutes} />

								{/*Routes for Communication menu*/}
								<Route path={`${path}/communication`} component={Communication} />

								{/*Routes for control center menu*/}
								<Route path={`${path}/control-center`} component={ControlCenterAdminRoutes} />

								<Route path={`*`} component={Page404} />
							</Switch>
						</main>


						<Footer />
					</Suspense>
				</div>
			</div>
		</Fragment>
	)
}

export default Main;

