import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = () => {
    const { path } = useRouteMatch();
    return (
        <>
            <table className="table">
                <thead>
                    <tr>
                        <th>IMG</th>
                        <th>Product Name</th>
                        <th>Customer Name</th>
                        <th>Stars</th>
                        <th>Status</th>

                        <th>actions</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span className="avatar avatar-lg" style={{ backgroundImage: 'url(/assets/images/xyz.jpg)' }} /></td>
                        <td>Product name goes here 1</td>
                        <td>	Customer goes here 1</td>

                        <td>3</td>

                        <td>
                            <span className="badge badge-success">published</span>
                        </td>

                        <td>
                            <Link to={`${path}/view`} className="btn btn-md "><i className="fas fa-eye" /></Link>
                            <Link className="btn btn-md "><i className="fas fa-trash-alt" /></Link>

                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    )
}

export default List