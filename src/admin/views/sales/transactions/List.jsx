import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = ({ match }) => {
    const { path } = useRouteMatch();
    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Order Id</th>
                        <th>Transaction Id</th>
                        <th>Payment Method</th>
                        <th>Transaction Total</th>
                        <th>created</th>

                        <th>action</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>#ODR0015</td>
                        <td>ch_1Eb4lJH4A8LFMNZXuleUUUET</td>
                        <td>Paypal</td>
                        <td>	765</td>
                        <td>6 hours ago</td>


                        <td>
                            <Link to={`${path}/transactionDetails`} className="btn btn-sm btn-info"><i className="fas fa-eye"></i></Link>
                        </td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default List