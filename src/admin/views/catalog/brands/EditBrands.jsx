import React, { Fragment, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useForm, ErrorMessage } from "react-hook-form";
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';
import { CustomInput, Form, CustomTextAreaInput, CustomCheckbox, AlertBox } from '../../../components/Inputs';
import { ParentUrl } from '../../../components/CommonHelpers'
import { useLocation } from 'react-router'

const EditBrands = ({ match }) => {
    const location = useLocation()
    const parentUrl = ParentUrl(match.path)
    const catalogContext = useContext(CatalogContext)


    const onSubmit = (values) => {
        if (location.state.type) {
            catalogContext.updateBrand(values, location.state.brand.id, location.state.type)
        }
        catalogContext.updateBrand(values, location.state.brand.id);

    }

    useEffect(() => {
        return () => {
            catalogContext.setSuccessMessage(null)
            catalogContext.setErrorMessage(null)
        }
    }, [])

    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    {catalogContext.successMessage && <AlertBox message={catalogContext.successMessage} type="alert-success" />}
                    {catalogContext.errorMessage && <AlertBox message={catalogContext.errorMessage} type="alert-danger" />}

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Edit brand: {location.state.brand.name}
                            </h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-align-justify mr-2"></i>View all Brand</Link>

                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Brand information</h3>
                        </div>
                        <div className="card-body">
                            <Form onSubmit={onSubmit} values={location.state.brand}>
                                <div className="row">
                                    <div className="col-md-6">
                                        <CustomInput
                                            label="Brand Name"
                                            placeholder="Enter brand name"
                                            name="name"
                                            type="text"

                                            required
                                            minLength="10"
                                        />
                                        <CustomTextAreaInput
                                            label="Brand short description"
                                            placeholder="Enter barnd short description between 50-160 characters"
                                            name="description"
                                            rows="6"
                                            type="text"
                                            required
                                            maxLength="100"
                                        />
                                        <CustomInput
                                            label="Brand origin"
                                            placeholder="Enter brand origin"
                                            name="origin"
                                            type="text"
                                            required
                                            minLength="2"
                                        />
                                        <CustomCheckbox
                                            name="status"
                                            label="Publish now?"
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <CustomInput
                                            label="SEO meta keywords"
                                            placeholder="keywords, sperated, by, comma"
                                            name="seo_keyword"
                                            type="text"
                                            required
                                            minLength="10"
                                        />
                                        <CustomTextAreaInput
                                            label="SEO meta description"
                                            placeholder="Enter barnd meta description between 50–160 characters." name="seo_meta_description"
                                            type="text"
                                            rows="6"
                                            rules={{
                                                minLength: { value: 50, message: "SEO meta description should have atleast 50 character" },
                                                maxLength: { value: 160, message: "SEO meta description should not exceed 160 character" },
                                            }}
                                        />
                                    </div>

                                </div>
                                <div className="text-right">
                                    <button type="submit" className="btn btn-md btn-primary">Save Changes</button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment >
    )
}

export default EditBrands