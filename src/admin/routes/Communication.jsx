import React from 'react';
import { Route, useRouteMatch, useHistory, Switch } from 'react-router-dom';

import { ContactRequests, archivedContactRequest, EditContactRequest, ComingSoonSubscribers, NewsletterSubscribers } from '../views/communication/Communication';

import Page404 from '../views/errorPages/page404';

const Communication = () => {
    const { path } = useRouteMatch();
    // const history = useHistory();
    // const pathName = history.location.pathname;
    return (
        <Switch>
            {/**
             * contact request
             */}
            <Route exact path={`${path}/contact-request`} component={ContactRequests} />
            <Route exact path={`${path}/contact-request/archived`} component={archivedContactRequest} />
            <Route exact path={`${path}/contact-request/edit`} component={EditContactRequest} />
            {/**
             * Coming soon subscribers 
             */}
            <Route exact path={`${path}/coming-soon-subscribers`} component={ComingSoonSubscribers} />
            {     /**
              * newsletterSubscribers
              */}
            <Route exact path={`${path}/newsletter-subscribers`} component={NewsletterSubscribers} />

            <Route path={`*`} component={Page404} />

        </Switch>
    )
}
export default Communication;