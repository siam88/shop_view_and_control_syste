import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = () => {
    const { path } = useRouteMatch();

    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Email</th>
                        <th>status</th>


                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>16-06-2019</td>
                        <td>customer@gmail.com</td>

                        <td>
                            <span className="badge badge-warning">new</span>
                        </td>

                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default List