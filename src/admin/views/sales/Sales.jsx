//orders
export { default as Orders } from './orders/Orders'
export { default as AddOrders } from './orders/AddOrders'
export { default as CanceledOrders } from './orders/CanceledOrders'
export { default as EditOrder } from './orders/EditOrder'

//transactions

export { default as Transactions } from './transactions/Transactions'
export { default as TransactionsDetails } from './transactions/TransactionsDetails'
//Invoices

export { default as Invoices } from './invoices/Invoices'
export { default as EditInvoice } from './invoices/EditInvoice'
//Coupons

export { default as Coupons } from './coupons/Coupons'
export { default as AddCoupons } from './coupons/AddCoupons'
export { default as EditCoupons } from './coupons/EditCoupons'
export { default as ArchivedCoupons } from './coupons/ArchivedCoupons'

//PromotionsAndDeal

export { default as PromotionsAndDeal } from './promotions/Promotions'
export { default as AddPromotionsAndDeal } from './promotions/AddPromotion'
export { default as Banner } from './promotions/Banner'
export { default as ArchivedPromotionsAndDeals } from './promotions/ArchivedPromotion'
export { default as EditPromotionsAndDeal } from './promotions/EditPromotion'
//Customers

export { default as Customers } from './customers/Customers'
export { default as AddCustomer } from './customers/AddCustomer'
export { default as BlockCustomers } from './customers/BlockCustomers'
export { default as EditCustomer } from './customers/EditCustomer'
export { default as CustomerAccount } from './customers/CustomerAccount'



