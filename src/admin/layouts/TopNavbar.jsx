import React, { Fragment, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../contexts/AuthContextProvider';


const TopNavbar = () => {

	const authContext = useContext(AuthContext);
	const user = authContext.authUser;

	const logout = () => {
		authContext.handleLogout();
	}
	return (
		<Fragment>
			<div className="header bg-white py-3">

				<div className="container">
					<div className="d-flex">
						<Link className="header-brand" to="/shop-admin">
							<img src="/assets/images/logo.jpeg" className="header-brand-img" alt="magic shop" />
						</Link>
						<div className="d-flex order-lg-2 ml-auto">
							<div className="nav-item d-none d-md-flex"> <Link to="/" className="btn btn-sm btn-outline-primary" target="_blank">Storefront</Link>
							</div>

							<div className="dropdown d-none d-md-flex">
								<button type="button" className="btn btn-outline-secondary btn-sm dropdown-toggle" data-toggle="dropdown"> <i className="fas fa-globe"></i> </button>
								<div className="dropdown-menu">
									<a className="dropdown-item" href="#">English</a>
									<a className="dropdown-item" href="#">Dutch</a>
								</div>
								<button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
									<span className="navbar-toggler-icon">
										<i className="fas fa-bars" style={{ color: "grey", fontSize: "28px" }}></i>
									</span>
								</button>
							</div>
							<div className="dropdown">
								<a href="#" className="user-dropdown leading-none pr-0" data-toggle="dropdown" aria-expanded="false">
									<span className="avatar" style={{ backgroundImage: "url(" + '/assets/images/user.jpg' + ")" }} />
									<span className="user-info ml-2 d-none d-lg-block">
										<span className="text-secondary">{user.name}</span>
										<small className="text-muted d-block mt-1">Administrator</small>
									</span>
								</a>
								<div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow" x-placement="bottom-end" style={{ position: 'absolute', transform: 'translate3d(-56px, 32px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
									<a className="dropdown-item" href="./ms_edit_system_user.html"> <i className="dropdown-icon fe fe-user" /> Profile </a>
									<div className="dropdown-divider" />
									<a className="dropdown-item" href="#"> <i className="dropdown-icon fe fe-help-circle" /> Need help? </a> <button className="dropdown-item" href="#" onClick={logout}> <i className="dropdown-icon fe fe-log-out" /> Sign out </button> </div>
								<form method="post" id="logout-form" action="http://localhost/magic-shop/shop-admin/logout" style={{ display: 'none' }}>
									<input type="hidden" name="_token" defaultValue="Vo4mLpHhnatjJjeAarLNYj32VyixPAEL6CbYqbxd" />    </form>
							</div>
							<button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
								<span className="navbar-toggler-icon">
									<i className="fas fa-bars" style={{ color: "grey", fontSize: "28px" }}></i>
								</span>
							</button>
						</div>
					</div>
					{/*container*/}
				</div>
			</div>
		</Fragment>
	)
}

export default TopNavbar