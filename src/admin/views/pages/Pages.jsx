/*Blog pages*/

export { default as Blog } from './blog/Blog'
export { default as AddBlog } from './blog/AddBlog'
export { default as ArchivedBlogCategory } from './blog/ArchivedBlogCategory'
export { default as BlogCategory } from './blog/Category'
export { default as AddBlogCategory } from './blog/AddBlogCategory'
export { default as BlogCategoryArchived } from './blog/ArchivedBlogCategory'
export { default as EditBlogCategory } from './blog/EditBlogCategory'
export { default as EditBlogArticle } from './blog/EditBlogArticle'

/*Faq pages*/
export { default as Faq } from './faq/Faq'
export { default as AddFaq } from './faq/AddFaq'
export { default as FaqCategory } from './faq/Category'
export { default as AddFaqCategory } from './faq/AddFaqCategory'
export { default as ArchivedFaqCategory } from './faq/ArchivedFaqCategory'
export { default as EditFaqCategory } from './faq/EditFaqCategory'
export { default as EditFaq } from './faq/EditFaq'
export { default as ArchivedFaqItems } from './faq/ArchivedFaqItems'

/*About us pages*/

export { default as AboutUs } from './aboutUs/AboutUs'
export { default as TeamSection } from './aboutUs/TeamSection'
export { default as TeamMemberAdd } from './aboutUs/TeamMemberAdd'
/*Contact us pages*/

export { default as ContactUs } from './contactUs/ContactUs'

//TermsAndConditions

export { default as TermsAndConditions } from './termsAndConditions/termsAndConditions'

//PrivacyPolicy

export { default as PrivacyPolicy } from './privacyPolicy/privacyPolicy'

//ReturnPolicy

export { default as ReturnPolicy } from './returnPolicy/returnPolicy'

