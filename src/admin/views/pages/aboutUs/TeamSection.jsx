import React from 'react';
import {Link} from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';

const TeamSection = ({match}) => {
    return (
        <>
            <div className="row">
                <div className="col-sm-12">
        
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage team section</h3>
                            <div className="card-options">
                                <Link to={`${ParentUrl(match.path)}`} className="btn btn-primary ml-2"><i className="fas fa-reply mr-2"></i></Link>
                                <Link to={`${match.path}/add`} className="btn btn-primary ml-2"><i className="fas fa-user-plus mr-2"></i>Add team member</Link>
                            </div>
                        </div>
                    </div>
        
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Team member list</h3>
                        </div>
                        <div className="card-body">
                            
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default TeamSection
