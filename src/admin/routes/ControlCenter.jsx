import React from 'react';
import { Route, useRouteMatch, useHistory, Switch } from 'react-router-dom';

import { TrackingScripts, SystemUserRoles, EditSystemUser, SystemUsers, PaymentGateways, ShippingOptions, TaxSettings, StoreInformation, EmailSetting, Localization, LanguageSettings, CurrencySettings } from '../views/controlCenter/ControlCenter';

import Page404 from '../views/errorPages/page404';


const ControlCenter = () => {
	const { path } = useRouteMatch();
	// const history = useHistory();
	// const pathName = history.location.pathname;

	return (
		<Switch>
			{/**store information*/}
			<Route exact path={`${path}/store-information`} component={StoreInformation} />
			<Route exact path={`${path}/email-settings`} component={EmailSetting} />
			<Route exact path={`${path}/localization`} component={Localization} />
			<Route exact path={`${path}/language-settings`} component={LanguageSettings} />
			<Route exact path={`${path}/currency-settings`} component={CurrencySettings} />
			<Route exact path={`${path}/tax-settings`} component={TaxSettings} />
			<Route exact path={`${path}/shipping-options`} component={ShippingOptions} />
			<Route exact path={`${path}/payment-gateways`} component={PaymentGateways} />
			<Route exact path={`${path}/system-users`} component={SystemUsers} />
			<Route exact path={`${path}/system-users/edit`} component={EditSystemUser} />
			<Route exact path={`${path}/system-user-roles`} component={SystemUserRoles} />
			<Route exact path={`${path}/tracking-scripts`} component={TrackingScripts} />
			<Route path={`*`} component={Page404} />
		</Switch>
	)
}

export default ControlCenter