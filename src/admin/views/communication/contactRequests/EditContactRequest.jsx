import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers'

const EditContactRequest = ({ match }) => {
    const parentUrl = ParentUrl(match.path)
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Replying to: Customer first name and last name</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-paper-plane mr-2"></i>View all requests</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Compose new message
</h3>
                        </div>
                        <div className="card-body">
                            <form action="">
                                <div className="form-group">
                                    <div className="row align-items-center">
                                        <label className="col-sm-2">To:</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row align-items-center">
                                        <label className="col-sm-2">Subject:</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <textarea rows="10" className="form-control"></textarea>
                                <div className="row mt-4">

                                    <div className="col-6">

                                        <div className="form-group">
                                            <div className="form-label">Mark as replied?</div>
                                            <label className="custom-switch">
                                                <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                <span className="custom-switch-indicator"></span> </label>
                                        </div>
                                    </div>
                                    <div className="col-6 ">
                                        <div className="text-right">
                                            <button type="submit" className="btn btn-lg btn-primary">Send message</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
export default EditContactRequest