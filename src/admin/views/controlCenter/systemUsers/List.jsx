import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = ({ match }) => {
    const { path } = useRouteMatch();
    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Img</th>
                        <th>User Name</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Created date</th>
                        <th>Status</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span className="avatar avatar-lg" style={{ backgroundImage: 'url(/assets/images/xyz.jpg)' }} /></td>
                        <td>User full name goes here</td>
                        <td><span className="badge ">admin</span></td>
                        <td>roryamp@dayrep.com</td>
                        <td>	03/24/2018		</td>
                        <td><span className="badge badge-success">active</span></td>


                        <td>
                            <Link to={`${path}/edit`} className="btn btn-md "><i className="fas fa-edit" /></Link>



                        </td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default List;