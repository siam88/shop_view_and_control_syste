import React, { Fragment, useContext } from 'react';
import { useForm, ErrorMessage } from "react-hook-form";

import styles from './controlPannelLogin.module.css'
import { AuthContext } from '../../../contexts/AuthContextProvider';
import { Redirect, Link } from 'react-router-dom';

const ControlPannelLogin = () => {

    const authContext = useContext(AuthContext);
    const { handleSubmit, register, errors } = useForm();


    const onSubmit = values => {
        authContext.handleLogin(values);
    };

    if (authContext.login) return <Redirect to="/shop-admin" />
    // console.log(authContext.login);
    return (
        <Fragment>
        <div className="container" style={{ background: "linear-gradient(to bottom, #f4f9ff 0%, #ffffff 100%)" }}>
            <div className="row justify-content-md-center">
                <div className="col-sm-4 " style={{ marginTop: "10%" }}>
                    <div className="card text-center" >
                        <div className="card-header">
                        <Link to="/" title="Go to home">
                            <img src="/assets/images/logo.jpeg" alt="Magic Shop" style={{ height: "4rem " }} />
                        </Link>
                            Login to your account
                        </div>
                        
                        { authContext.errorMsg ?
                            <div className="alert alert-danger fade in alert-dismissible show">
                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true" style={{fontSize: '20px'}}>×</span>
                                </button>
                                <strong>Error!</strong> {authContext.errorMsg}
                            </div>
                        : '' }

                        { authContext.logoutMsg ?
                            <div className="alert alert-success fade in alert-dismissible show">
                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true" style={{fontSize: '20px'}}>×</span>
                                </button>
                                <strong>Success!</strong> {authContext.logoutMsg}
                            </div>
                        : '' }

                        <div className="card-body">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="form-group"  >
                                    <div className="row justify-content-between">
                                        <div className="col-8">
                                            <label className={styles.formLabel}>Email address</label>
                                        </div>
                                    </div>
                                    <input
                                        type="email"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        placeholder="Enter email"
                                        name="email"
                                        ref={register({
                                            required: 'Email is Required',
                                            pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                message: "* invalid email address"
                                            }
                                        })}
                                    />
                                    <ErrorMessage className="text-danger small" errors={errors} name="email" as="p" />
                                </div>

                                <div className="form-group" >
                                    <div className="row justify-content-between">
                                        <div className="col-6">
                                            <label className={styles.formLabel}>Password </label>
                                        </div>
                                        <div className="col-6">
                                            <a href="#" className="float-right small">I forgot password</a>
                                        </div>
                                    </div>
                                    <input
                                        className="form-control"
                                        placeholder="Password"
                                        type="password"
                                        id="password"
                                        name="password"
                                        ref={register({
                                            required: '* password is required',
                                            minLength: {
                                                value: 5, message: "* minimum length 5"
                                            },

                                        })}
                                    />

                                    <ErrorMessage className="text-danger small" errors={errors} name="password" as="p" />
                                </div>

                                <div className="form-group" style={{ marginTop: '1rem' }}>
                                    <input type="checkbox" />
                                    <span >Remember me</span>
                                </div>
                                <button type="submit" className="btn btn-primary btn-block">Log in</button>
                            </form>
                        </div>
                        <div className="form-footer">
                            {/*third party login options*/}
                            <Link to="/">Go to home page</Link>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
export default ControlPannelLogin;