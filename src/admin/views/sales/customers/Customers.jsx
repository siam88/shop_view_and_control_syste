import React from 'react';
import { Link } from 'react-router-dom';
import List from './List';

const Customers = ({ match }) => {
    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage customers</h3>
                            <div className="card-options">
                                <Link to={`${match.path}/add`} className="btn btn-primary ml-2"><i className="fas fa-plus mr-2"></i>Add Customer</Link>
                                <Link to={`${match.path}/blockedList`} className="btn btn-outline-danger ml-2"><i className="fas fa-archive mr-2"></i>View blocked customers</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Customer list</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}

export default Customers