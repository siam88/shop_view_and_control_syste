import React from 'react'
import { Link } from 'react-router-dom'
import List from './list'

const Orders = ({ match }) => {
    console.log('match', match)
    return (
        <>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage Orders</h3>
                            <div className="card-options">
                                <Link to={`${match.path}/add`} className="btn btn-primary ml-2">
                                    <i className="fas fa-plus-square mr-2" />
                                   Add order
                                </Link>
                                <Link to={`${match.path}/canceledOrders`} className="btn btn-outline-danger ml-2">
                                    <i className="fas fa-exclamation-triangle mr-2" />
                                    View canceled orders
                                </Link>
                                <button type="button" className="btn btn-outline-dark ml-2" data-toggle="modal" data-target="#orderSettingsModal">
                                    <i className="fas fa-cog mr-2" />
                                    Order Settings
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Order Lists</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>

                    { /*-- Modal Order Settings--*/}
                    <form id="orderSettings">
                        <div className="modal fade" id="orderSettingsModal" tabIndex="-1" role="dialog" aria-labelledby="orderSettingsModal" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="orderSettingsModalLabel">Order settings</h5>
                                    </div>
                                    <div className="modal-body">
                                        <div className="form-group mt-6">
                                            <label className="form-label">Order Number Prefix <span className="form-required">*</span></label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="ORD-" />
                                        </div>
                                        <div className="form-group mt-6">
                                            <label className="form-label">Starting number<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="100" />
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" className="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}



export default Orders;