import React, { Fragment, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { useForm, ErrorMessage } from "react-hook-form";
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';
import { CustomInput, Form, CustomTextAreaInput, CustomCheckbox, AlertBox } from '../../../components/Inputs';

import { ParentUrl } from '../../../components/CommonHelpers'
import FileUpload from '../../../components/FileUpload';

const AddCategories = ({ match }) => {
    const { handleSubmit, register, errors } = useForm();
    const parentUrl = ParentUrl(match.path);
    const catalogContext = useContext(CatalogContext);


    const onSubmit = values => {
        catalogContext.createCategory(values);
    }
    useEffect(() => {
        return () => {
            catalogContext.setSuccessMessage(null)
            catalogContext.setErrorMessage(null)
        }
    }, [])
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    {catalogContext.successMessage && <AlertBox message={catalogContext.successMessage} type="alert-success" />}
                    {catalogContext.errorMessage && <AlertBox message={catalogContext.errorMessage} type="alert-danger" />}

                    <div className="card">

                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Add category</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-align-justify mr-2"></i>View all categories</Link>

                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Category information</h3>
                        </div>
                        <div className="card-body">
                            <Form onSubmit={onSubmit} >
                                <div className="row">
                                    <div className="col-md-6">

                                        <CustomInput
                                            label="Category name"
                                            placeholder="Enter category name"
                                            name="name"
                                            type="text"
                                            required
                                            minLength="10"
                                        />
                                        <CustomTextAreaInput
                                            label="Category short description"
                                            placeholder="Enter category  description between 50-160 characters"
                                            name="description"
                                            rows="6"
                                            type="text"
                                            required
                                            maxLength="160"
                                            minLength="50"
                                        />

                                        <CustomCheckbox
                                            name="status"
                                            label="Make it active?"
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <CustomInput
                                            label="SEO meta keywords"
                                            placeholder="keywords, sperated, by, comma"
                                            name="seo_keyword"
                                            type="text"
                                            minLength="10"
                                        />
                                        <CustomTextAreaInput
                                            label="SEO meta description"
                                            placeholder="Enter barnd meta description between 50–160 characters."
                                            name="seo_meta_description"
                                            rows="6"
                                            type="text"
                                            maxLength="100"
                                            minLength="50"
                                        />


                                    </div>

                                </div>
                                <div className="text-right">
                                    <button type="submit" className="btn btn-md btn-primary">Add category</button>
                                </div>
                            </Form>

                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default AddCategories






