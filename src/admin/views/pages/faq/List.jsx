import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const Table = () => {
	const { path } = useRouteMatch();
	return (
		<Fragment>
			<table className="table">
				<thead>
					<tr>
						<th>Question</th>
						<th>FAQ Category</th>
						<th>Added Date</th>
						<th>Published</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Question name goes here 1</td>
						<td>FAQ Category name here</td>
						<td>01-06-2019</td>
						<td>
							<span className="badge badge-success">published</span>
						</td>
						<td>
							<Link to={`${path}/edit`} className="btn btn-sm "><i className="fas fa-edit"></i></Link>
							<Link to="" className="btn btn-sm "><i className="fas fa-archive"></i></Link>
							<Link to="" className="btn btn-sm "><i className="fas fa-eye-slash"></i></Link>
						</td>
					</tr>
				</tbody>
			</table >
		</Fragment >
	)
}

export default Table