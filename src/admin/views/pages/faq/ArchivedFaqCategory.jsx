import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';
import FaqCategoryList from './FaqCategoryList';
import { ParentRoot, ParentUrl } from '../../../components/CommonHelpers';

const Category = ({ match }) => {
	const parentUrl = ParentUrl(match.path);
	const parentRoot = ParentRoot(match.path);
	console.log('ParentUrl', parentUrl)
	console.log('parentroot', parentRoot)

	return (
		<Fragment>
			<div className="row">
				<div className="col-sm-12">

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header page-title-card p-4">
							<h3 className="card-title">Manage FAQ categories</h3>
							<div className="card-options">
								<Link to={`${parentRoot}`} className="btn btn-primary"><i className="fas fa-reply"></i></Link>
								<Link to={`${parentUrl}/add`} className="btn btn-primary ml-2"><i className="fas fa-plus-square mr-2"></i>Add faq category </Link>
								<Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-list mr-2"></i>View all Faq categories</Link>
							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-6">
							<h3 className="card-title">FAQ archived category list</h3>
						</div>
						<div className="card-body">
							<FaqCategoryList />
						</div>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default Category