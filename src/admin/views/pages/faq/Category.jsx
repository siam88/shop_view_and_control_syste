import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';
import FaqCategoryList from './FaqCategoryList';

const Category = ({ match }) => {
	const parentUrl = match.path.slice(0, match.path.lastIndexOf("/"));
	return (
		<Fragment>
			<div className="row">
				<div className="col-sm-12">

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header page-title-card p-4">
							<h3 className="card-title">Manage FAQ categories</h3>
							<div className="card-options">
								<Link to={`${parentUrl}`} className="btn btn-primary"><i className="fas fa-reply"></i></Link>
								<Link to={`${match.path}/add`} className="btn btn-primary ml-2"><i className="fas fa-plus-square mr-2"></i>Add faq category </Link>
								<Link to={`${match.path}/archived`} className="btn btn-outline-info ml-2"><i className="fas fa-archive mr-2"></i>View archived faq categories</Link>
							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-6">
							<h3 className="card-title">FAQ category list</h3>
						</div>
						<div className="card-body">
							<FaqCategoryList />
						</div>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default Category