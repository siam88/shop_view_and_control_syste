import React from 'react';
import List from './List'

const Transactions = ({ match }) => {

    return (
        <>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-body p-6">

                            <div className="alert alert-primary">
                                Only succesful transactions will be registered.
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Add Blog</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Transactions;