import React from 'react';
import { Link } from 'react-router-dom';

const ContactUs = ({ match }) => {
	return (
		<>
			<div className="row">
				<div className="col-sm-12">
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert"></button>
            Show message after succesful CRUD </div>
					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header page-title-card p-4">
							<h3 className="card-title">Page contact us</h3>
							<div className="card-options">
								<Link to={`${match.path}`} className="btn btn-primary ml-2"><i className="fas fa-cog mr-2"></i>Store settings</Link>
							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-6">
							<h3 className="card-title">Contact us</h3>
						</div>
						<div className="card-body">
							<form id="pageAboutus">
								<div class="row">
									<div class="col-12">
										<div class="form-group">
											<label class="form-label">System page name(not allowed to change due to automatic links)</label>
											<input type="text" disabled="" class="form-control" placeholder="Contact us" value="Contact us" />
										</div>
										<div class="form-group">
											<label class="form-label">Custom page name (SEO Slug) <span class="form-required">*</span></label>
											<input type="text" class="form-control" placeholder="Contact us" value="" />
										</div>
										<div class="form-group">
											<label class="form-label">SEO meta keywords <span class="form-required">*</span></label>
											<input type="text" class="form-control" placeholder="keywords, sperated, by, comma" />
										</div>
										<div class="form-group">
											<label class="form-label">SEO meta description <span class="form-required">*</span></label>
											<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Enter product meta description between 50–160 characters. "></textarea>
										</div>

										<div class="form-group">
											<label class="form-label">Page header <span class="form-required">*</span></label>
											<input type="text" class="form-control" placeholder="About us page header" value="Smartphones in Every Day Life" />
										</div>
										<div class="form-group">
											<label class="form-label">Page content<span class="form-required">*</span></label>
											<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="This needs to be replaced by Summernote"></textarea>
										</div>
									</div>

								</div>
								<div class="row">
									<div class="col-12 p-3">
										<div class="form-group">
											<label class="form-label">Current page images</label>
											<div class="row gutters-sm">
												<div class="col-sm-2">
													<label class="imagecheck mb-4">
														<input name="imagecheck" type="checkbox" value="1" class="imagecheck-input" />
														<figure class="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" class="imagecheck-image" /> </figure>
													</label>
												</div>
												<div class="col-sm-2">
													<label class="imagecheck mb-4">
														<input name="imagecheck" type="checkbox" value="2" class="imagecheck-input" checked />
														<figure class="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" class="imagecheck-image" /> </figure>
													</label>
												</div>
												<div class="col-sm-2">
													<label class="imagecheck mb-4">
														<input name="imagecheck" type="checkbox" value="3" class="imagecheck-input" />
														<figure class="imagecheck-figure"> <img src="/assets/images/nature.jpg" alt="}" class="imagecheck-image" /> </figure>
													</label>
												</div>



											</div>
										</div>
									</div>

								</div>
								<div class="row mt-4 mb-4">
									<div class="col-6"> </div>
									<div class="col-6">
										<div class="text-right">
											<button type="submit" class="btn btn-lg btn-danger">Delete slected images</button>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<div class="form-group">
											<div class="form-label">Add new page images (multiple selections are allowed)</div>
											<div class="custom-file">
												<input type="file" class="custom-file-input" name="example-file-input-custom" />
												<label class="custom-file-label">Choose images</label>
											</div>
										</div>
                    (image previews after upload)
                    <div class="row row-cards row-deck mt-4 mb-4">
											<div class="col-sm-6 col-xl-3 p-2"> <img class="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
											<div class="col-sm-6 col-xl-3 p-2"> <img class="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
											<div class="col-sm-6 col-xl-3 p-2"> <img class="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
											<div class="col-sm-6 col-xl-3 p-2"> <img class="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
										</div>
									</div>
								</div>
								<div class="row mt-4">
									<div class="col-6"> </div>
									<div class="col-6">
										<div class="text-right">
											<button type="submit" class="btn btn-lg btn-primary">Save changes</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default ContactUs