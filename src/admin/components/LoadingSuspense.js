import React from 'react';

const LoadingSuspense = () => {
    return (
        <div className="container">
            <div className="row align-self-center">
                <div className="col-7 ml-auto">
                    <div className="spinner-grow row align-items-center justify-content-center" style={{ width: "5rem", height: "5rem", role: "status" }}>
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default LoadingSuspense;