





import React, { useContext, Fragment, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';
import LoadingSuspense from '../../../components/LoadingSuspense';

const List = () => {
    const catelogContext = useContext(CatalogContext)
    const { path } = useRouteMatch();


    async function getChildCategories() {
        const status = await catelogContext.getChildCategories();
        if (!status) console.log("error getting datas");
    }
    const handleArchive = (category) => {
        catelogContext.addArchivedChildCategory(category);

    }
    useEffect(() => {
        getChildCategories();
    }, [])

    return (
        <Fragment>
            {catelogContext.childCategories ?
                <div className="table-responsive">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Child Category Name</th>
                                <th>Belongs to Category</th>
                                <th>Product Count</th>
                                <th>Published</th>
                                <th>actions</th>

                            </tr>
                        </thead>
                        <tbody>
                            {catelogContext.childCategories.map((e, i) =>
                                <tr key={i}>
                                    <td>{e.name}</td>
                                    <td>{e.category_id}</td>
                                    <td>100</td>
                                    <td>
                                        {
                                            e.status ? <span className="badge badge-success">published</span> : <span className="badge badge-danger">unpublished</span>
                                        }
                                    </td>
                                    <td>
                                        <Link to={{
                                            pathname: `${path}/edit`,
                                            state: { childCategory: e }
                                        }} className="btn btn-md "><i className="far fa-edit" /></Link>
                                        <Link tp="#" className="btn btn-md " onClick={() => handleArchive(e)}><i className="far fa-hdd" /></Link>
                                        <Link to="#" className="btn btn-md "><i className="far fa-eye-slash" /></Link>
                                    </td>
                                </tr>


                            )}

                        </tbody>
                    </table>
                </div> : <LoadingSuspense />}
        </Fragment>
    )
}

export default List




