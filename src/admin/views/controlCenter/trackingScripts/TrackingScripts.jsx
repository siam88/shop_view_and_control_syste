import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';

const TrackingScripts = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Tracking scripts</h3>
                            <div className="card-options">
                                <button className="btn btn-primary ml-2" type="button" data-toggle="modal" data-target="#addShippingModal" ><i className="fas fa-plus mr-2"></i>Store tracking scripts</button>

                                <Link to={`${parentUrl}/store-information`} className="btn btn-outline-dark ml-2"><i className="fas fa-info mr-2"></i>Store information</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Shipping information</h3>
                        </div>
                        <div className="card-body">
                            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" id="pills-analytics-tab" data-toggle="pill" href="#pills-analytics" role="tab" aria-controls="pills-analytics" aria-selected="true">Google</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-facebook-tab" data-toggle="pill" href="#pills-facebook" role="tab" aria-controls="pills-facebook" aria-selected="false">Facebook</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-twitter-tab" data-toggle="pill" href="#pills-twitter" role="tab" aria-controls="pills-twitter" aria-selected="false">Twitter</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-custom-tab" data-toggle="pill" href="#pills-custom" role="tab" aria-controls="pills-custom" aria-selected="false">Custom</a>
                                </li>
                            </ul>
                            <div className="tab-content p-4" id="pills-tabContent">
                                <div className="tab-pane fade show active" id="pills-analytics" role="tabpanel" aria-labelledby="pills-analytics-tab">

                                    <form id="googleAnalytics">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Scripts in header <span className="form-label-small">will be places in the head section</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="script goes here"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Scripts in footer <span className="form-label-small">will be placed just above the closing body tag</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="script goes here"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-facebook" role="tabpanel" aria-labelledby="pills-facebook-tab">

                                    <form id="facebookPixel">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Scripts in header <span className="form-label-small">will be places in the head section</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="script goes here"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Scripts in footer <span className="form-label-small">will be placed just above the closing body tag</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="script goes here"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                    <form id="twitterCard">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Scripts in header <span className="form-label-small">will be places in the head section</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="script goes here"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Scripts in footer <span className="form-label-small">will be placed just above the closing body tag</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="script goes here"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-custom" role="tabpanel" aria-labelledby="pills-custom-tab">
                                    <form id="customCode">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Scripts in header <span className="form-label-small">will be places in the head section</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="script goes here"></textarea>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Scripts in footer <span className="form-label-small">will be placed just above the closing body tag</span></label>
                                                    <textarea className="form-control" name="example-textarea-input" rows="6" placeholder="script goes here"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-4">
                                            <div className="col-6"> </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </Fragment>
    )
}

export default TrackingScripts