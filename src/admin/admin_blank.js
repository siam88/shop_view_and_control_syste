<Fragment>
	<div className="row">
		<div className="col-sm-12">
			<div className="card">
				<div className="card-status bg-primary"></div>
				<div className="card-header page-title-card p-4">
					<h3 className="card-title">Manage Blogs</h3>
					<div className="card-options">
						<Link to={`${match.path}/add`} className="btn btn-primary ml-2"><i className="fas fa-plus mr-2"></i>Add blog</Link>
						<Link to={`${match.path}/archived`} className="btn btn-outline-info ml-2"><i className="fas fa-archive mr-2"></i>Archived blogs</Link>
					</div>
				</div>
			</div>

			<div className="card">
				<div className="card-status bg-primary"></div>
				<div className="card-header p-6">
					<h3 className="card-title">Add Blog</h3>
				</div>
				<div className="card-body">
					
				</div>
			</div>
		</div>
	</div>
</Fragment>













const MainAppRoutes = () => (
    <Switch>
        <Route exact path='/' component={HomePage} />
        {CustomerAppRoutes()}
        {CarAppRoutes()}
    </Switch>
);

const CustomerAppRoutes = () => ([
    <Route path='/customers' component={CustomersDisplayPage} />,
    <Route path='/customers/:id' component={CustomersDisplayPage} />
]);

const CarAppRoutes = () => ([
    <Route path='/cars' component={CarDisplayPage} />,
    <Route path='/cars/:id' component={CarDisplayPage} />
]);
