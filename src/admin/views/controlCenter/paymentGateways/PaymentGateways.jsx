import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';

const ShippingOptions = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Payment gateways</h3>
                            <div className="card-options">


                                <Link to={`${parentUrl}/store-information`} className="btn btn-outline-dark ml-2"><i className="fas fa-info mr-2"></i>Store information</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Payments gateway settings</h3>
                        </div>
                        <div className="card-body">
                            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" id="pills-banktransfer-tab" data-toggle="pill" href="#pills-banktransfer" role="tab" aria-controls="pills-banktransfer" aria-selected="true">Bank transfer</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-paypal-tab" data-toggle="pill" href="#pills-paypal" role="tab" aria-controls="pills-paypal" aria-selected="false">Paypal</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-stripe-tab" data-toggle="pill" href="#pills-stripe" role="tab" aria-controls="pills-stripe" aria-selected="false">Stripe</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-ideal-tab" data-toggle="pill" href="#pills-ideal" role="tab" aria-controls="pills-ideal" aria-selected="false">iDeal(mollie)</a>
                                </li>
                            </ul>
                            <div className="tab-content p-4" id="pills-tabContent">
                                <div className="tab-pane fade show active" id="pills-banktransfer" role="tabpanel" aria-labelledby="pills-banktransfer-tab">
                                    <form id="bank">
                                        <div className="row">
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Bank name</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="Bank of store account" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">BIC</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="BIC" />
                                                </div>


                                            </div>
                                            <div className="col-md-6">

                                                <div className="form-group">
                                                    <label className="form-label">IBAN account nr</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="IBAN account nr" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Quick notes</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="Your order will be sent when the transaction is complete." />
                                                </div>

                                            </div>
                                        </div>
                                        <div className="row mt-4">

                                            <div className="col-6">
                                                <div className="form-group">
                                                    <div className="form-label">Active?</div>
                                                    <label className="custom-switch">
                                                        <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                        <span className="custom-switch-indicator"></span> </label>
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div className="tab-pane fade" id="pills-paypal" role="tabpanel" aria-labelledby="pills-paypal-tab">
                                    <form id="paypal">
                                        <div className="row">
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Label</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="Paypal" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">PayPal API Password</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="PayPal API Password" />
                                                </div>


                                            </div>
                                            <div className="col-md-6">

                                                <div className="form-group">
                                                    <label className="form-label">PayPal API Username</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="PayPal API Username" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">API Signature</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="API Signature" />
                                                </div>

                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className="form-label">Quick notes</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="PayPal - the safer, easier way to pay" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-3">
                                                <div className="form-group">
                                                    <div className="form-label">Active?</div>
                                                    <label className="custom-switch">
                                                        <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                        <span className="custom-switch-indicator"></span> </label>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="form-group">
                                                    <div className="form-label">Test mode?</div>
                                                    <label className="custom-switch">
                                                        <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="" />
                                                        <span className="custom-switch-indicator"></span> </label>
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>


                                </div>
                                <div className="tab-pane fade" id="pills-stripe" role="tabpanel" aria-labelledby="pills-stripe-tab">

                                    <form id="stripe">
                                        <div className="row">
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Label</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="Stripe" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Stripe Publishable Key</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="Stripe Publishable Key" />
                                                </div>


                                            </div>
                                            <div className="col-md-6">

                                                <div className="form-group">
                                                    <label className="form-label">Stripe API Secret Key</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="Stripe API Secret Key" />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Quick notes</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="We accept following credit cards: Mastercard, Visa" />
                                                </div>

                                            </div>
                                        </div>

                                        <div className="row mt-4">

                                            <div className="col-3">
                                                <div className="form-group">
                                                    <div className="form-label">Active?</div>
                                                    <label className="custom-switch">
                                                        <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                        <span className="custom-switch-indicator"></span> </label>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="form-group">
                                                    <div className="form-label">Test mode?</div>
                                                    <label className="custom-switch">
                                                        <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="" />
                                                        <span className="custom-switch-indicator"></span> </label>
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="tab-pane fade" id="pills-ideal" role="tabpanel" aria-labelledby="pills-ideal-tab">

                                    <form id="ideal">
                                        <div className="row">
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6"> </div>
                                            <div className="col-md-6">
                                                <div className="form-group">
                                                    <label className="form-label">Label</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="iDeal" />
                                                </div>



                                            </div>
                                            <div className="col-md-6">

                                                <div className="form-group">
                                                    <label className="form-label">API Key</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="API Key" />
                                                </div>


                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label className="form-label">Quick notes</label>
                                                    <input type="text" className="form-control" name="example-text-input" placeholder="DEAL is seamlessly integrated with the online banking products of the 10 largest Dutch consumer banks.  " />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mt-4">

                                            <div className="col-3">
                                                <div className="form-group">
                                                    <div className="form-label">Active?</div>
                                                    <label className="custom-switch">
                                                        <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                        <span className="custom-switch-indicator"></span> </label>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="form-group">
                                                    <div className="form-label">Test mode?</div>
                                                    <label className="custom-switch">
                                                        <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="" />
                                                        <span className="custom-switch-indicator"></span> </label>
                                                </div>
                                            </div>
                                            <div className="col-6">
                                                <div className="text-right">
                                                    <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>






        </Fragment>
    )
}

export default ShippingOptions