import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import TopNavbar from './TopNavbar';
import { useRouteMatch, useHistory } from 'react-router-dom';
const Navbar = (props) => {
	// const {path} = useRouteMatch();
	// const history = useHistory();
	// const pathName = history.location.pathname;
	const path = props.path;
	const pathArr = props.currentPath.split('/');
	const matchUrl = pathArr[1] + '/' + pathArr[2];
	// console.log('path', path, ', pathArr', pathArr, ', matchUrl', matchUrl)
	return (
		<Fragment>
			<TopNavbar />

			<nav className="navbar navbar-expand-lg sticky-top navbar-white bg-white p-0">
				<div className="container">
					{/*<a className="navbar-brand" >Fixed navbar</a>*/}
					{/* <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	                <span className="navbar-toggler-icon">
	                 <i class="fas fa-bars" style={{"color":"red", "font-size":"28px"}}></i>
	                </span>
	              </button>*/}
					<div className="collapse navbar-collapse" id="navbarCollapse">
						<div className="col-lg-3 ml-auto" style={{ paddingRight: 0 }}>
							<form className="form-inline">
								<div className="input-group">
									<input type="search" className="form-control border-right-0 border search-input pr-0" placeholder="Search…" tabIndex="1" />
									<div className="input-group-append">
										<button className="btn border-left-0 border" type="submit"><i className="fas fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>

						<ul className="navbar-nav order-lg-first">
							<li className={`nav-item ${props.currentPath == '/shop-admin' ? 'active' : ''}`}>
								<NavLink className="nav-link" to={`${path}`}>Dashboard <span className="sr-only">(current)</span></NavLink>
							</li>
							<li className={`nav-item dropdown ${matchUrl == 'shop-admin/catalog' ? 'active' : ''}`}>
								<a className="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i className="fas fa-shopping-cart mr-1"></i>
	                      		Catalog
	                    		</a>
								<div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<NavLink to={`${path}/catalog/products`} className="dropdown-item" >Products</NavLink>
									<NavLink to={`${path}/catalog/categories`} className="dropdown-item" >Categories</NavLink>
									<NavLink to={`${path}/catalog/child-categories`} className="dropdown-item" >Child Categories</NavLink>
									<NavLink to={`${path}/catalog/brands`} className="dropdown-item" >Brands</NavLink>
									<NavLink to={`${path}/catalog/reviews`} className="dropdown-item" >Reviews</NavLink>

								</div>
							</li>
							<li className={`nav-item dropdown ${matchUrl == 'shop-admin/sales' ? 'active' : ''}`}>
								<a className="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i className="fas fa-dollar-sign mr-1"></i>
	                      Sales
	                    </a>
								<div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<NavLink to={`${path}/sales/orders`} className="dropdown-item" >Orders</NavLink>
									<NavLink to={`${path}/sales/transactions`} className="dropdown-item" >Transactions</NavLink>
									<NavLink to={`${path}/sales/invoices`} className="dropdown-item" >Invoices</NavLink>
									<NavLink to={`${path}/sales/coupons`} className="dropdown-item" >Coupons</NavLink>
									<NavLink to={`${path}/sales/promotions`} className="dropdown-item" >promotions/deals</NavLink>
									<NavLink to={`${path}/sales/customers`} className="dropdown-item" >Customers</NavLink>

								</div>
							</li>
							<li className={`nav-item dropdown ${matchUrl == 'shop-admin/pages' ? 'active' : ''}`}>
								<a className="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i className="fas fa-book-open mr-1"></i>
	                      Pages
	                    </a>
								<div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<NavLink to={`${path}/pages/blog`} className="dropdown-item" >Blog</NavLink>
									<NavLink to={`${path}/pages/faq`} className="dropdown-item" >FAQ</NavLink>
									<NavLink to={`${path}/pages/about-us`} className="dropdown-item" >About us</NavLink>
									<NavLink to={`${path}/pages/contact-us`} className="dropdown-item" >Contact us</NavLink>
									<NavLink to={`${path}/pages/terms-and-conditions`} className="dropdown-item" >Terms and condotions</NavLink>
									<NavLink to={`${path}/pages/privacy-policy`} className="dropdown-item" >Privacy policy</NavLink>
									<NavLink to={`${path}/pages/return-policy`} className="dropdown-item" >Return policy</NavLink>

								</div>
							</li>
							<li className={`nav-item dropdown ${matchUrl == 'shop-admin/communication' ? 'active' : ''}`}>
								<a className="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i className="fas fa-envelope mr-1"></i>
	                      Communication
	                    </a>
								<div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<NavLink to={`${path}/communication/contact-request`} className="dropdown-item" >Contact request</NavLink>
									<NavLink to={`${path}/communication/coming-soon-subscribers`} className="dropdown-item" >Coming Soon Subscribers</NavLink>
									<NavLink to={`${path}/communication/newsletter-subscribers`} className="dropdown-item" >Newsletter Subscribers</NavLink>

								</div>
							</li>
							{/*<li className="nav-item dropdown">
								<a className="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i className="fas fa-tv mr-1"></i>
			                      Home Pages
			                    </a>
								<div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<NavLink to={`${path}/home-page/home1`} className="dropdown-item" >Home 1</NavLink>
								</div>
							</li>*/}
							<li className={`nav-item dropdown ${matchUrl == 'shop-admin/control-center' ? 'active' : ''}`}>
								<a className="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i className="fas fa-cogs mr-1"></i>
			                      Control Center
			                    </a>
								<div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<NavLink to={`${path}/control-center/store-information`} className="dropdown-item" >Store Information</NavLink>
									<NavLink to={`${path}/control-center/email-settings`} className="dropdown-item" >Email Settings</NavLink>
									<NavLink to={`${path}/control-center/localization`} className="dropdown-item" >Localization</NavLink>
									<NavLink to={`${path}/control-center/language-settings`} className="dropdown-item" >Language Settings</NavLink>
									<NavLink to={`${path}/control-center/currency-settings`} className="dropdown-item" >Currency Settings</NavLink>
									<NavLink to={`${path}/control-center/tax-settings`} className="dropdown-item" >Tax Settings</NavLink>
									<NavLink to={`${path}/control-center/shipping-options`} className="dropdown-item" >Shipping Options</NavLink>
									<NavLink to={`${path}/control-center/payment-gateways`} className="dropdown-item" >Payment Gateways</NavLink>
									<NavLink to={`${path}/control-center/system-users`} className="dropdown-item" >System Users</NavLink>
									<NavLink to={`${path}/control-center/system-user-roles`} className="dropdown-item" >System User Roles</NavLink>
									<NavLink to={`${path}/control-center/tracking-scripts`} className="dropdown-item" >Tracking Scripts</NavLink>

								</div>
							</li>

						</ul>
						{/*<form className="form-inline mt-2 mt-md-0">
	                  <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
	                  <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
	                </form>*/}
					</div>
				</div>
			</nav>
		</Fragment>
	)
}

export default Navbar