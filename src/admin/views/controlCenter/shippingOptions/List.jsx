import React, { Fragment } from 'react';

const List = ({ match }) => {
    return (
        <Fragment>
            <form id="vatSettings">
                <div className="table-responsive">
                    <table className="table mb-0">
                        <thead>
                            <tr>
                                <th className="pl-0">Name</th>
                                <th>Cost </th>

                                <th className="pr-0">Active</th>
                            </tr>
                        </thead>
                        <tr>
                            <td className="pl-0">
                                <input type="text" className="form-control" placeholder="Free shipping" value="" />
                            </td>
                            <td>
                                <input type="text" className="form-control" placeholder="0" value="0" /></td>

                            <td className="pr-0 pt-4">
                                <div className="custom-controls-stacked">
                                    <label className="custom-control custom-radio custom-control-inline">
                                        <input type="radio" className="custom-control-input" name="example-inline-radios" value="option1" checked />
                                        <span className="custom-control-label">activate</span>
                                    </label>


                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td className="pl-0">
                                <input type="text" className="form-control" placeholder="UPS ground" value="" />
                            </td>
                            <td>
                                <input type="text" className="form-control" placeholder="15" value="15" /></td>

                            <td className="pr-0 pt-4">
                                <div className="custom-controls-stacked">

                                    <label className="custom-control custom-radio custom-control-inline">
                                        <input type="radio" className="custom-control-input" name="example-inline-radios" value="option3" />
                                        <span className="custom-control-label">activate</span>
                                    </label>
                                </div>

                            </td>
                        </tr>


                    </table>
                </div>
                <div className="row mt-4">
                    <div className="col-6"> </div>
                    <div className="col-6">
                        <div className="text-right">
                            <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </form>
        </Fragment>
    )
}

export default List