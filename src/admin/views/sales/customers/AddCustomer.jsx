import React from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers'

const AddCustomer = ({ match }) => {
    const parentUrl = ParentUrl(match.path)
    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="alert alert-success alert-dismissible">
                        <button type="button" className="close" data-dismiss="alert"></button>
            Show message after succesful CRUD </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Add customer</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-users mr-2"></i>View all customer</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Customer information</h3>
                        </div>
                        <div className="card-body">
                            <form id="addCategory">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Customer name<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter customer name" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Customer email address<span className="form-required">*</span></label>
                                            <input type="email" className="form-control" placeholder="Email" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">

                                        <div className="form-group">
                                            <label className="form-label">Customer phone<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Enter customer phone" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Password</label>
                                            <input type="password" className="form-control" name="example-password-input" placeholder="Password.." />
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <div className="form-group">
                                            <div className="form-label">Activate now?</div>
                                            <label className="custom-switch">
                                                <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                <span className="custom-switch-indicator"></span> </label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="text-right">
                                            <button type="submit" className="btn btn-md btn-primary">Add customer</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}

export default AddCustomer;