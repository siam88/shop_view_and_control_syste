import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios';
import { ADMIN_API_URL } from '../Config';

export const AuthContext = createContext();


const AuthContextProvider = ({ children }) => {
    const [errorMsg, setErrorMsg] = useState('');
    const [logoutMsg, setLogoutMsg] = useState('');
    const [isLogin, setIsLogin] = useState(false);


    const [authUser, setAuthUser] = useState(() => {
        const item = localStorage.getItem('logins');
        return item ? JSON.parse(item) : false;
    });

    const [login, setLogin] = useState(() => {
        try {
            const item = localStorage.getItem('logins');
            const login = item ? JSON.parse(item) : false;
            return login.isLogedIn ? true : false;
        } catch (error) {
            return false;
        }
    });

    useEffect(() => {
        const item = localStorage.getItem('logins');
        const logins = item ? JSON.parse(item) : false;
        setAuthUser(logins);
    }, [login]);


    const handleLogout = () => {
        axios.post(`${ADMIN_API_URL}/logout`)
            .then((res) => {
                let success = res.data.success;
                if (success) {
                    setLogoutMsg(res.data.message);
                    localStorage.removeItem('logins');
                    setLogin(false)
                } else {
                    setLogoutMsg('Logout error!');
                }
            });
    }

    const handleLogin = (formData) => {
        setIsLogin(true);
        axios.post(`${ADMIN_API_URL}/login`, formData)
            .then((res) => {
                setIsLogin(false);
                let success = res.data.success;
                if (success) {
                    // console.log(res.data.user);
                    let user = res.data.user;
                    let guard = res.data.guard;
                    let access_token = res.data.access_token;
                    const logins = {
                        isLogedIn: true,
                        name: user.name,
                        email: user.email,
                        role: user.role,
                        image: user.image,
                        guard: guard,
                        authToken: access_token
                    }

                    localStorage.setItem('logins', JSON.stringify(logins));
                    window.location.href = "/shop-admin";
                } else {
                    setErrorMsg('You have entered an invalid username or password');
                    // console.log("Login: ", res.data.message);
                }
            });

        // document.cookie = `logins=${JSON.stringify(logins)}; expires=26 Apr 2020 23:59:59 GMT; SameSite=strict; Secure=true`;
        // const cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)logins\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    }



    const values = {
        login,
        handleLogin,
        isLogin,
        handleLogout,
        authUser,
        errorMsg,
        logoutMsg
    }

    return (
        <AuthContext.Provider value={values} >
            {children}
        </AuthContext.Provider>
    );
}

export default AuthContextProvider;