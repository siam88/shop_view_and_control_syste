import React from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers'
const Banner = ({ match }) => {
    const parentUrl = ParentUrl(match.path)
    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="alert alert-success alert-dismissible">
                        <button type="button" className="close" data-dismiss="alert" />
                        Show message after succesful CRUD
                    </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Promotion banner
</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-tag mr-2"></i>View All Promotions</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Promotion banner information
</h3>
                        </div>
                        <div className="card-body">
                            <form id="promotionBanner">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Banner name<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Special offer" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Regular price<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="800" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Banner description<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="FLOS Indoor Lighting" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Discounted price<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="560" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className=" col-12"> Current banner background image
                    <div className="card"> <img className="card-img-top" src="/assets/images/logo192.png" alt="footer background" height="200px" /> </div>
                                        <div className="form-group">
                                            <div className="form-label">Upload new background image</div>
                                            <div className="custom-file">
                                                <input type="file" className="custom-file-input" name="example-file-input-custom" />
                                                <label className="custom-file-label">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <div className="form-group">
                                            <label className="form-label">Banner link<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="url" />
                                        </div>
                                    </div>
                                    <div className=" col-6"> Current banner product image
                    <div className="card"> <img className="card-img-top" src="/assets/images/currrentBannerProduct.jpeg" alt="footer background" height="250px " width="50px" /> </div>
                                        <div className="form-group">
                                            <div className="form-label">Upload new product image</div>
                                            <div className="custom-file">
                                                <input type="file" className="custom-file-input" name="example-file-input-custom" />
                                                <label className="custom-file-label">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-6">
                                        <div className="form-group">
                                            <div className="form-label">Make it active?</div>
                                            <label className="custom-switch">
                                                <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                <span className="custom-switch-indicator"></span> </label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="text-right">
                                            <button type="submit" className="btn btn-md btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Banner;