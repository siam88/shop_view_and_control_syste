import React from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers'
import List from './list';

const ArchivedPromotionsAndDeals = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    return (
        <div>
            <>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-status bg-primary"></div>
                            <div className="card-header page-title-card p-4">
                                <h3 className="card-title">Manage promotions and deals
</h3>
                                <div className="card-options">
                                    <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-tag mr-2"></i>View all categories</Link>
                                </div>
                            </div>
                        </div>

                        <div className="card">
                            <div className="card-status bg-primary"></div>
                            <div className="card-header p-6">
                                <h3 className="card-title">Archived Promotions and deals list
</h3>
                            </div>
                            <div className="card-body">
                                <List />
                            </div>
                        </div>
                    </div>
                </div>
            </>
        </div>
    )
}

export default ArchivedPromotionsAndDeals