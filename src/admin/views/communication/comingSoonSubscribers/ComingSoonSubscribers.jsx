import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import List from './List';

const ComingSoonSubscriber = ({ match }) => {
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage coming soon subscribers</h3>
                            <div className="card-options">
                                <Link to="" className="btn btn-primary ml-2"><i className="fas fa-cloud-download-alt mr-2"></i>Export</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Coming soon subscribers list</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
export default ComingSoonSubscriber;