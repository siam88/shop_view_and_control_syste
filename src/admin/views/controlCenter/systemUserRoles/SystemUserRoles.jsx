import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';

const CurrencySettings = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage system user roles</h3>
                            <div className="card-options">

                                <Link to={`${parentUrl}/store-information`} className="btn btn-outline-dark ml-2"><i className="fas fa-info mr-2"></i>Store information</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Permissions</h3>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-lg-4">
                                    <div className="card p-3">
                                        <div className="d-flex align-items-center"> <span className="stamp stamp-md bg-success mr-3"> <i className="fa fa-shield"></i> </span>
                                            <div>
                                                <h4 className="m-0">Admin</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="adminRole">
                                        <div className="form-group">
                                            <div className="form-label">Catalog</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Produts</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Categories</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Child categories</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Brands</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Reviews</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Sales</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Orders</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Transactions</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Invoices</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Coupons</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Promotions and deals</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Customers</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Pages</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Blog</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">FAQ</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">About us</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Contact us</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Terms and conditions</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Privacy policy</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Returns policy</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Communication</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Contact requests</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Coming soon subscribers</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Newsletter subscribers</span> </label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Home pages</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Home page 1</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Home page 2</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Home page 3</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Control center</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Store information</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Email settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Localization</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Language settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Currency settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Tax settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Shipping options</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Payment gateways</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">System users</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">System user roles</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Tracking scripts</span> </label>
                                            </div>
                                        </div>
                                        <div className="card-footer text-right">
                                            <button type="submit" className="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-4">
                                    <div className="card p-3">
                                        <div className="d-flex align-items-center"> <span className="stamp stamp-md bg-info mr-3"> <i className="fa fa-shield"></i> </span>
                                            <div>
                                                <h4 className="m-0">Store manager</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="managerRole">
                                        <div className="form-group">
                                            <div className="form-label">Catalog</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Produts</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Categories</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Child categories</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Brands</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Reviews</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Sales</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Orders</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Transactions</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Invoices</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Coupons</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Promotions and deals</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Customers</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Pages</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Blog</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">FAQ</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">About us</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Contact us</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Terms and conditions</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Privacy policy</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Returns policy</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Communication</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Contact requests</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Coming soon subscribers</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Newsletter subscribers</span> </label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Home pages</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Home page 1</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Home page 2</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Home page 3</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Control center</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Store information</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Email settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Localization</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Language settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Currency settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Tax settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Shipping options</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Payment gateways</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">System users</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">System user roles</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Tracking scripts</span> </label>
                                            </div>
                                        </div>
                                        <div className="card-footer text-right">
                                            <button type="submit" className="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-4">
                                    <div className="card p-3">
                                        <div className="d-flex align-items-center"> <span className="stamp stamp-md bg-warning mr-3"> <i className="fa fa-shield"></i> </span>
                                            <div>
                                                <h4 className="m-0">Store employee</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="employeeRole">
                                        <div className="form-group">
                                            <div className="form-label">Catalog</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Produts</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Categories</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Child categories</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Brands</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Reviews</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Sales</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Orders</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Transactions</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Invoices</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Coupons</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Promotions and deals</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Customers</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Pages</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Blog</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">FAQ</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">About us</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Contact us</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Terms and conditions</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Privacy policy</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Returns policy</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Communication</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" checked />
                                                    <span className="custom-control-label">Contact requests</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Coming soon subscribers</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Newsletter subscribers</span> </label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Home pages</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Home page 1</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Home page 2</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Home page 3</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group">
                                            <div className="form-label">Control center</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Store information</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Email settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Localization</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Language settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Currency settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Tax settings</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Shipping options</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Payment gateways</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">System users</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">System user roles</span> </label>
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" name="example-checkbox1" value="option1" />
                                                    <span className="custom-control-label">Tracking scripts</span> </label>
                                            </div>
                                        </div>
                                        <div className="card-footer text-right">
                                            <button type="submit" className="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default CurrencySettings