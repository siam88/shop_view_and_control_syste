

import React, { useContext, Fragment, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';
import LoadingSuspense from '../../../components/LoadingSuspense';

const List = () => {
    const catelogContext = useContext(CatalogContext)
    const { path } = useRouteMatch();


    async function getBrands() {
        const status = await catelogContext.getCategories();
        if (!status) console.log("error getting datas");
    }

    const handleArchive = (category) => {
        catelogContext.addArchivedCategory(category);

    }

    useEffect(() => {
        getBrands();
    }, [])

    return (
        <Fragment>
            {catelogContext.categories ?
                <div className="table-responsive">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>IMG</th>
                                <th>CATEGORY NAME</th>
                                <th>CATEGORY DESC</th>
                                <th>PRODUCT COUNT</th>
                                <th>Published</th>

                                <th>ACTIONS</th>

                            </tr>
                        </thead>
                        <tbody>
                            {catelogContext.categories.map((e, i) =>
                                <tr key={i}>
                                    <td><span className="avatar avatar-lg" style={{ backgroundImage: 'url(/assets/images/xyz.jpg)' }} /></td>
                                    <td>{e.name}</td>
                                    <td>{e.description}</td>
                                    <td>100</td>
                                    <td>
                                        {
                                            e.status ? <span className="badge badge-success">published</span> : <span className="badge badge-danger">unpublished</span>
                                        }
                                    </td>
                                    <td>
                                        <Link to={{
                                            pathname: `${path}/edit`,
                                            state: { category: e }
                                        }} className="btn btn-md "><i className="far fa-edit" /></Link>
                                        <Link to="#" className="btn btn-md " onClick={() => handleArchive(e)}><i className="far fa-hdd" /></Link>
                                        <Link to="#" className="btn btn-md "><i className="far fa-eye-slash" /></Link>
                                    </td>
                                </tr>


                            )}

                        </tbody>
                    </table>
                </div> : <LoadingSuspense />}
        </Fragment>
    )
}

export default List









