import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = ({ match }) => {
    const { path } = useRouteMatch();
    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Img</th>
                        <th>Customer Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Created date</th>
                        <th>Status</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span className="avatar avatar-lg" style={{ backgroundImage: 'url(/assets/images/xyz.jpg)' }} /></td>
                        <td>Customer name goes here 1</td>
                        <td>078 5054 8877</td>
                        <td>roryamp@dayrep.com</td>
                        <td>	03/24/2018		</td>
                        <td><span className="badge badge-success">Published</span></td>


                        <td>
                            <Link to={`${path}/edit`} className="btn btn-md "><i className="fas fa-edit" /></Link>
                            <Link to="" className="btn btn-md "><i className="fas fa-user-lock" /></Link>
                            <Link to={`${path}/account`} className="btn btn-md "><i className="fas fa-user" /></Link>


                        </td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default List