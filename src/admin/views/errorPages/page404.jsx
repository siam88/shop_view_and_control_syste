import React, { Fragment } from 'react';
import { Link, } from "react-router-dom"
import styles from './page404.module.css';

const Page404 = () => {

    return (


        <Fragment>
            <div id={styles.notfound}>
                <div class={styles.notfound}>
                    <div class={styles.notfound404}>
                        <div></div>
                        <h1>404</h1>
                    </div>
                    <h2>Page not found</h2>
                    <p>The page you are looking for might have been removed had its name changed or is temporarily unavailable.</p>
                    <Link to={'/shop-admin'} className="btn btn-primary ml-2">Home Page</Link>

                </div>
            </div>
        </Fragment>
    )

}
export default Page404;