import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import List from './list';
import { ParentUrl } from '../../../components/CommonHelpers'

const AddBlog = ({ match }) => {
    const parentUrl = ParentUrl(match.path);



    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-4">
                            <h3 className="card-title">Manage Orders</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary"><i className="fas fa-tag mr-2"></i>View all orders</Link>

                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header">
                            <h3 className="card-title">Canceled Orders</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default AddBlog