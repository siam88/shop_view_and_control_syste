import React from 'react';
import { Link } from 'react-router-dom';
import List from './list';

const PromotionsAndDeals = ({ match }) => {
    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage promotions and deals</h3>
                            <div className="card-options">
                                <Link to={`${match.path}/add`} className="btn btn-primary ml-2"><i className="fas fa-plus mr-2"></i>Add promotions</Link>
                                <Link to={`${match.path}/banner`} className="btn btn-dark ml-2"><i className="fas fa-ad me-2" /> Promotions banner</Link>

                                <Link to={`${match.path}/archived`} className="btn btn-outline-info ml-2"><i className="fas fa-archive mr-2"></i>View archived promotions</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Promotions and deals list</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )

}

export default PromotionsAndDeals