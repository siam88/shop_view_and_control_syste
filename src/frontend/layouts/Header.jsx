import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
	return (
		<Fragment>
			<nav className="navbar navbar-expand-lg navbar-light bg-light">
				<div className="container">
					{/*<a className="navbar-brand" href="#">Navbar</a>*/}
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span className="navbar-toggler-icon"></span>
					</button>
					<div className="collapse navbar-collapse" id="navbarNav">
						<ul className="navbar-nav">
							<li className="nav-item active">
								<NavLink to="/" className="nav-link">Home</NavLink>
							</li>
							<li className="nav-item">
								<NavLink to="/shop/about-us" className="nav-link">About us</NavLink>
							</li>
							<li className="nav-item">
								<NavLink to="/shop/contact-us" className="nav-link">Contact us</NavLink>
							</li>
							<li className="nav-item">
								<NavLink to="/shop/gallery" className="nav-link">Gallery</NavLink>
							</li>
							<li className="nav-item">
								<NavLink to="/shop-admin" className="nav-link">Admin Dashboard</NavLink>
							</li>

						</ul>
					</div>
				</div>
			</nav>
		</Fragment>
	)
}

export default Header