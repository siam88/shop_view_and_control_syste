import React from 'react';
import { Route, useRouteMatch, useHistory, Switch } from 'react-router-dom';



import { Products, EditProduct, ArchivedProduct, AddProduct, Categories, AddCategories, ArchivedCategories, EditCategories, ChildCategory, AddChildCategory, ArchivedChildCategory, EditChildCategory, Brands, AddBrands, EditBrands, ArchivedBrands, Reviews, ViewReview } from '../views/catalog/Catalog'

//error
import Page404 from '../views/errorPages/page404';

const Catalog = () => {
	const { path } = useRouteMatch();
	// const history = useHistory();
	// const pathName = history.location.pathname;

	return (
		<Switch>
			{/**Products*/}
			<Route exact path={`${path}/products`} component={Products} />
			<Route exact path={`${path}/products/edit`} component={EditProduct} />
			<Route exact path={`${path}/products/add`} component={AddProduct} />
			<Route exact path={`${path}/products/archived`} component={ArchivedProduct} />

			<Route exact path={`${path}/categories`} component={Categories} />
			<Route exact path={`${path}/categories/add`} component={AddCategories} />
			<Route exact path={`${path}/categories/archived`} component={ArchivedCategories} />
			<Route exact path={`${path}/categories/edit`} component={EditCategories} />

			<Route exact path={`${path}/child-categories`} component={ChildCategory} />
			<Route exact path={`${path}/child-categories/add`} component={AddChildCategory} />
			<Route exact path={`${path}/child-categories/archived`} component={ArchivedChildCategory} />
			<Route exact path={`${path}/child-categories/edit`} component={EditChildCategory} />
			<Route exact path={`${path}/brands`} component={Brands} />
			<Route exact path={`${path}/brands/add`} component={AddBrands} />
			<Route exact path={`${path}/brands/archived`} component={ArchivedBrands} />
			<Route exact path={`${path}/brands/edit`} component={EditBrands} />
			<Route exact path={`${path}/brands/archived/edit`} component={EditBrands} />
			<Route exact path={`${path}/reviews`} component={Reviews} />
			<Route path={`${path}/reviews/view`} component={ViewReview} />

			<Route path={`*`} component={Page404} />
		</Switch>
	)
}

export default Catalog