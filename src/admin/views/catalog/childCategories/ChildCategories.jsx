import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import ListTable from './List';

const ChildCategories = ({ match }) => {
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage child categories
</h3>
                            <div className="card-options">
                                <Link to={`${match.path}/add`} className="btn btn-primary ml-2"><i className="fas fa-plus mr-2"></i>Add child category</Link>
                                <Link to={`${match.path}/archived`} className="btn btn-outline-info ml-2"><i className="far fa-hdd mr-2"></i>View archived  child categories</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Child category list</h3>
                        </div>
                        <div className="card-body">
                            <ListTable />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default ChildCategories;