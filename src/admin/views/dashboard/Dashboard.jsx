import React, {Fragment} from 'react'

const Dashboard = () => {
	return (
		<Fragment>
      <div className="page-header">
  <div>
    <h1 className="page-title">Dashboard</h1>
    
  </div>
</div>
      <div className="row">
 
        <div className="col-md-3">
          <div className="card p-2 px-3">
            <div>
              Total revenue
            </div>
            <div className="py-3 m-0 text-center h1 text-green">$14,320</div>
            <div className="d-flex">
              <small className="text-muted">This year</small>

            </div>
          </div>
        </div>
        <div className="col-md-3">
          <div className="card p-2 px-3">
            <div>
              Total products
            </div>
            <div className="py-3 m-0 text-center h1 text-primary">738</div>
            <div className="d-flex">
              <small className="text-muted">All active products</small>

            </div>
          </div>
        </div>
        <div className="col-md-3">
          <div className="card p-2 px-3">
            <div>
              Registred customers
            </div>
            <div className="py-3 m-0 text-center h1 text-primary">1234</div>
            <div className="d-flex">
              <small className="text-muted">All active customers</small>
              
            </div>
          </div>
        </div>
        <div className="col-md-3">
          <div className="card p-2 px-3">
            <div>
              Failed transactions
            </div>
            <div className="py-3 m-0 text-center h1 text-danger">118</div>
            <div className="d-flex">
              <small className="text-muted">Failed transactions this year</small>

            </div>
          </div>
        </div>
        </div>


        <div className="row">
        
        <div className="col-md-6">
          
          <div className="card">
            <div className="card-header">
              <h3 className="card-title">
                Most recent orders
              </h3>
            </div>
            <table className="table card-table">
              <tbody><tr>
                <th>
                  Date
                </th>
                <th>
                  Country
                </th>
                <th className="text-right">
                  Amount
                </th>
              </tr>
              <tr>
                <td className="text-muted">
                  13.06.2017
                </td>
                <td>
                  Belgium
                </td>
                <td className="text-right">
                  $14,740
                </td>
              </tr>
              <tr>
                <td className="text-muted">
                  28.02.2017
                </td>
                <td>
                  USA
                </td>
                <td className="text-right">
                  $11,002
                </td>
              </tr>
              <tr>
                <td className="text-muted">
                  06.03.2017
                </td>
                <td>
                  Netherlands
                </td>
                <td className="text-right">
                  $10,900
                </td>
              </tr>
              <tr>
                <td className="text-muted">
                  21.10.2017
                </td>
                <td>
                  Finland
                </td>
                <td className="text-right">
                  $14,740
                </td>
              </tr>
              <tr>
                <td className="text-muted">
                  02.01.2017
                </td>
                <td>
                  UK
                </td>
                <td className="text-right">
                  $18,540
                </td>
              </tr>
              <tr>
                <td className="text-muted">
                  06.03.2017
                </td>
                <td>
                  Belgium
                </td>
                <td className="text-right">
                  $10,900
                </td>
              </tr>
              <tr>
                <td className="text-muted">
                  31.12.2017
                </td>
                <td>
                  Netherlands
                </td>
                <td className="text-right">
                  $25,609
                </td>
              </tr>
            </tbody></table>
            <div className="card-footer">
              <div className="text-right">
                <button type="button" className="btn btn-primary">
                Go to orders
              </button>
            </div>
          </div>
        </div>
        </div>
        <div className="col-md-6">
          
          <div className="card">
            <div className="card-header">
              <h3 className="card-title">
                New added products
              </h3>
            </div>
            <table className="table card-table">
              <tbody><tr>
                <th>
                  Date
                </th>
                <th>
                  Product
                </th>

              </tr>
              <tr>
                <td className="text-muted">
                  13.06.2017
                </td>
                <td>
                  Echo Dot (2nd Generation)
                </td>

              </tr>
              <tr>
                <td className="text-muted">
                  28.02.2017
                </td>
                <td>
                  Zeus Bluetooth Headphones
                </td>
              </tr>
              <tr>
                <td className="text-muted">
                  06.03.2017
                </td>
                <td>
                  Aberg Best 21 Mega Pixels
                </td>

              </tr>
              <tr>
                <td className="text-muted">
                  21.10.2017
                </td>
                <td>
                  Microsoft Surface Pro 4
                </td>
                
              </tr>
              <tr>
                <td className="text-muted">
                  02.01.2017
                </td>
                <td>
                  Sony 55-Inch 4K Smart TV
                </td>

              </tr>
              <tr>
                <td className="text-muted">
                  06.03.2017
                </td>
                <td>
                  Samsung Gear 360 Camera
                </td>

              </tr>
              <tr>
                <td className="text-muted">
                  31.12.2017
                </td>
                <td>
                  Edifier W855BT Bluetooth
                </td>
                
              </tr>
            </tbody></table>
            <div className="card-footer">
              <div className="text-right">
                <button type="button" className="btn btn-primary">
                Go to products
              </button>
            </div>
          </div>
        </div>
        </div>
        </div>

		</Fragment>
	)
}

export default Dashboard