export { default as Products } from './products/Products';
export { default as EditProduct } from './products/EditProduct';
export { default as ArchivedProduct } from './products/ArchivedProducts';
export { default as AddProduct } from './products/AddProduct';

export { default as Categories } from './categories/Categories';
export { default as AddCategories } from './categories/AddCategories';
export { default as ArchivedCategories } from './categories/ArchivedCategories';
export { default as EditCategories } from './categories/EditCategories';


export { default as ChildCategory } from './childCategories/ChildCategories';
export { default as AddChildCategory } from './childCategories/AddChildCategory';
export { default as ArchivedChildCategory } from './childCategories/ArchivedChildCategory';
export { default as EditChildCategory } from './childCategories/EditChildCategory';


export { default as Brands } from './brands/Brands';
export { default as AddBrands } from './brands/AddBrands';
export { default as ArchivedBrands } from './brands/ArchivedBrands';
export { default as EditBrands } from './brands/EditBrands';


export { default as Reviews } from './reviews/Reviews';
export { default as ViewReview } from './reviews/ViewReview';


