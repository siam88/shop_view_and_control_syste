import React from 'react';
import { Switch, useRouteMatch, useHistory, Route } from 'react-router-dom';

import { Orders, AddOrders, CanceledOrders, EditOrder, Transactions, TransactionsDetails, Invoices, EditInvoice, Coupons, AddCoupons, EditCoupons, ArchivedCoupons, PromotionsAndDeal, AddPromotionsAndDeal, Banner, ArchivedPromotionsAndDeals, EditPromotionsAndDeal, Customers, AddCustomer, BlockCustomers, EditCustomer, CustomerAccount } from '../views/sales/Sales';


//error
import Page404 from '../views/errorPages/page404';

const Sales = () => {
    const { path } = useRouteMatch();
    // const history = useHistory();
    // const pathName = history.location.pathname;

    return (
        <Switch>
            {/*START Sales*/}
            {/*START Orders */}
            <Route exact path={`${path}/orders`} component={Orders} />
            <Route exact path={`${path}/orders/add`} component={AddOrders} />
            <Route exact path={`${path}/orders/canceledOrders`} component={CanceledOrders} />
            <Route exact path={`${path}/orders/edit`} component={EditOrder} />
            {/*END Orders */}

            {/*START Transactions*/}
            <Route exact path={`${path}/transactions`} component={Transactions} />
            <Route exact path={`${path}/transactions/transactionDetails`} component={TransactionsDetails} />
            {/*END Transactions*/}

            {/*START Invoices*/}
            <Route exact path={`${path}/invoices`} component={Invoices} />
            <Route exact path={`${path}/invoices/edit`} component={EditInvoice} />
            {/*End Invoices*/}

            {/*START Coupons*/}
            <Route exact path={`${path}/coupons`} component={Coupons} />
            <Route exact path={`${path}/coupons/add`} component={AddCoupons} />
            <Route exact path={`${path}/coupons/edit`} component={EditCoupons} />
            <Route exact path={`${path}/coupons/archived`} component={ArchivedCoupons} />
            {/*End Coupons*/}

            {/*START Promotions/deal*/}
            <Route exact path={`${path}/promotions`} component={PromotionsAndDeal} />
            <Route exact path={`${path}/promotions/add`} component={AddPromotionsAndDeal} />
            <Route exact path={`${path}/promotions/banner`} component={Banner} />
            <Route exact path={`${path}/promotions/archived`} component={ArchivedPromotionsAndDeals} />
            <Route exact path={`${path}/promotions/edit`} component={EditPromotionsAndDeal} />
            {/*End Promotions/deal*/}

            {/*START Customers*/}
            <Route exact path={`${path}/customers`} component={Customers} />
            <Route exact path={`${path}/customers/add`} component={AddCustomer} />
            <Route exact path={`${path}/customers/blockedList`} component={BlockCustomers} />
            <Route exact path={`${path}/customers/edit`} component={EditCustomer} />
            <Route exact path={`${path}/customers/account`} component={CustomerAccount} />
            {/*End Customers*/}
            {/*END Sales*/}
            <Route path={`*`} component={Page404} />

        </Switch>
    )

}

export default Sales;