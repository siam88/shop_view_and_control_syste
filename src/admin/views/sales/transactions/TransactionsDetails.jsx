import React from "react";
import { Link } from 'react-router-dom'
import { ParentUrl } from '../../../components/CommonHelpers'

const TransactionsDetails = ({ match }) => {
    const parentUrl = ParentUrl(match.path)
    console.log("....", match.path)
    console.log("....", parentUrl)

    return (
        <>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Transaction Details</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-clipboard-list ml-2" /> View all transactions</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Transaction id: ch_1Eb4lJH4A8LFMNZXuleUUUET</h3>
                        </div>
                        <div className="card-body">
                            <div className="row p-5">
                                <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                        <div className="card-status bg-green"></div>
                                        <div className="card-body text-center">
                                            <div className="card-category">Customer</div>
                                            <h3> Customer Name</h3>

                                            <div className="text-center mt-6">
                                                <a href="./ms_customer_account.html" className="btn btn-secondary btn-block">View customer</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                        <div className="card-status bg-green"></div>
                                        <div className="card-body text-center">
                                            <div className="card-category">Order ID</div>
                                            <h3> #ODR0015</h3>


                                            <div className="text-center mt-6">
                                                <a href="./ms_edit_order.html" className="btn btn-secondary btn-block">View order</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                        <div className="card-status bg-green"></div>
                                        <div className="card-body text-center">
                                            <div className="card-category">Transaction Total</div>
                                            <h3 className="text-success">$99</h3>
                                            <i className="payment payment-paypal mt-6"></i>


                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                        <div className="card-status bg-green"></div>
                                        <div className="card-body text-center">
                                            <div className="card-category">Date-Time</div>
                                            <h3>19-05-2019</h3>
                                            <p className="mt-7">16:00</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default TransactionsDetails