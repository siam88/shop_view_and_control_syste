import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const AddBlog = ({ match }) => {
	const parentUrl = match.path.slice(0, match.path.lastIndexOf("/"));


	return (
		<Fragment>
			<div className="row">
				<div className="col-sm-12">
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert"></button>
            Show message after succesful CRUD </div>
					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-4">
							<h3 className="card-title">Add blog article</h3>
							<div className="card-options">
								<Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-book mr-2"></i>View all articles</Link>
							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header">
							<h3 className="card-title">Article information</h3>
						</div>
						<div className="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Article name<span class="form-required">*</span></label>
										<input type="text" class="form-control" placeholder="Enter article name" />
									</div>
									<div class="form-group">
										<label class="form-label">Article short description<span class="form-required">*</span></label>
										<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Enter article short description"></textarea>
									</div>
									<div class="form-group">
										<label class="form-label">Choose a blog category <span class="form-required">*</span></label>
										<select class="form-control custom-select">
											<option value="brand1">Category 1</option>
											<option value="brand2">Category 2</option>
											<option value="brand3">Category 3</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">SEO meta keywords</label>
										<input type="text" class="form-control" placeholder="keywords, sperated, by, comma" />
									</div>
									<div class="form-group">
										<label class="form-label">SEO meta description</label>
										<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Enter barnd meta description between 50–160 characters. "></textarea>
									</div>
									<div class="form-group">
										<label class="form-label">Tags</label>
										<input type="text" class="form-control" id="input-tags" value="aa,bb,cc,dd" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<div class="form-group">
										<label class="form-label">Article long description</label>
										<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Please replace textare with summernote "></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<div class="form-group">
										<div class="form-label">Add article images(multiple selections are allowed)</div>
										<div class="custom-file">
											<input type="file" class="custom-file-input" name="example-file-input-custom" />
											<label class="custom-file-label">Choose images</label>
										</div>
									</div>
                    (image previews after upload)
                    <div class="row row-cards row-deck mt-4 mb-4">
										<div class="col-sm-6 col-xl-3 p-2"> <img class="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
										<div class="col-sm-6 col-xl-3 p-2"> <img class="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
										<div class="col-sm-6 col-xl-3 p-2"> <img class="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
										<div class="col-sm-6 col-xl-3 p-2"> <img class="rounded" src="/assets/images/nature.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
									</div>
								</div>
							</div>
							<div class="row mt-4">
								<div class="col-6">
									<div class="form-group">
										<div class="form-label">Publish now?</div>
										<label class="custom-switch">
											<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" value="1" checked />
											<span class="custom-switch-indicator"></span> </label>
									</div>
								</div>
								<div class="col-6">
									<div class="text-right">
										<button type="submit" class="btn btn-lg btn-primary">Add blog article</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default AddBlog