import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = () => {
    const { path } = useRouteMatch();
    console.log("path", path)
    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Coupon Name</th>
                        <th>Code</th>
                        <th>Discount</th>
                        <th>Applied To</th>
                        <th>Time Used</th>
                        <th>Created</th>
                        <th>Status</th>
                        <th>Actions</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Name of coupon</td>
                        <td>5PERCENT</td>
                        <td>5%</td>
                        <td>Store wide</td>
                        <td>124</td>
                        <td>6 hours ago</td>

                        <td>
                            <span className="badge badge-success">Active</span>
                        </td>
                        <td>
                            <Link to={`${path}/edit`} className="btn btn-sm "><i className="fas fa-edit"></i></Link>
                            <Link to="" className="btn btn-sm "><i className="fas fa-archive"></i></Link>
                            <Link to="" className="btn btn-sm "><i className="fas fa-eye-slash"></i></Link>

                        </td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default List