import React, { Fragment } from 'react';

const List = ({ match }) => {
    return (
        <Fragment>
            <form id="vatSettings">
                <div className="table-responsive">
                    <table className="table mb-0">
                        <thead>
                            <tr>
                                <th className="pl-0">Name</th>
                                <th>Value (percent) </th>
                                <th className="pr-0">Active</th>
                                <th className="pr-0">Delete</th>
                            </tr>
                        </thead>
                        <tr>
                            <td className="pl-0"><input type="text" className="form-control" placeholder="High" value="" /></td>
                            <td><input type="text" className="form-control" placeholder="21" value="21" /></td>
                            <td className="pt-4">



                                <label className="custom-switch">
                                    <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                    <span className="custom-switch-indicator"></span>

                                </label>



                            </td>
                            <td className="pr-0 pt-4"><a className="icon " href="javascript:void(0)"> <i className="fe fe-trash-2" data-toggle="tooltip" title="delete"></i></a></td>
                        </tr>
                        <tr>
                            <td className="pl-0"><input type="text" className="form-control" placeholder="Low" value="" /></td>
                            <td><input type="text" className="form-control" placeholder="9" value="9" /></td>
                            <td className="pt-4">


                                <label className="custom-switch">
                                    <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                    <span className="custom-switch-indicator"></span>

                                </label>


                            </td>
                            <td className="pr-0 pt-4"><a className="icon " href="javascript:void(0)"> <i className="fe fe-trash-2" data-toggle="tooltip" title="delete"></i></a></td>
                        </tr>
                    </table>
                </div>
                <div className="row mt-4">
                    <div className="col-6"> </div>
                    <div className="col-6">
                        <div className="text-right">
                            <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </form>
        </Fragment>
    )
}

export default List