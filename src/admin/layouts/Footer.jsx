import React, {Fragment} from 'react'

const Footer = () => {
	return (
		<Fragment>
			<footer className="footer bg-white">
	          <div className="container">
	            <span className="text-muted">Place sticky footer content here.</span>
	          </div>
	        </footer>
		</Fragment>
	)
}

export default Footer