import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import ListTable from './List';

const Reviews = ({ match }) => {
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">

                            <div className="alert alert-success alert-dismissible">
                                <button type="button" className="close" data-dismiss="alert"></button>
                                Show message after succesful CRUD </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Review list</h3>
                        </div>
                        <div className="card-body">
                            <ListTable />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Reviews