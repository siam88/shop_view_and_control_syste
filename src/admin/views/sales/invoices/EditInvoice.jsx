import React from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from "../../../components/CommonHelpers"

const EditInvoice = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    console.log(parentUrl)
    return (
        <>
            <div className="row">

                <div className="col-sm-12">
                    <div className="alert alert-success alert-dismissible">
                        <button type="button" className="close" data-dismiss="alert"></button>
                    Show message after succesful CRUD
                    </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Edit Order Status</h3>
                            <div className="card-options">
                                <button type="button" className="btn btn-gray-dark ml-2" data-toggle="modal" data-target="#editInvoiceModal"><i className="fas fa-clock ml-2"></i> Change status</button>
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-file-invoice
                                 mr-2"></i>View all Invoices</Link>
                            </div>
                        </div>
                    </div>




                    <div className="card">
                        <div className="card-status bg-blue"></div>
                        <div className="card-header">
                            <h3 className="card-title">#INVC0015:<span className="tag tag-success ml-2">Paid</span> </h3>
                            <div className="card-options"> <a href="#" className="card-options-collapse" data-toggle="card-collapse"><i className="fe fe-chevron-up"></i></a> <a href="#" className="card-options-fullscreen" data-toggle="card-fullscreen"><i className="fe fe-maximize"></i></a> <a href="#" className="card-options" onclick="javascript:window.print();"><i className="fe fe-download"></i></a> <a href="#"><i className="fe fe-mail"></i></a> </div>
                        </div>
                        <div className="card-body">
                            <div className="p-5">
                                <div className="row">
                                    <div className="col text-right">

                                        {/*-- Badge --*/}
                                        <div className="badge badge-success"> Paid </div>
                                        {/*-- Text --*/}
                                        <p className="text-muted mb-6"> Invoice #SDF9823KD </p>
                                    </div>
                                </div>
                                {/*-- / .row --*/}
                                <div className="row">
                                    <div className="col text-center">

                                        {/*-- Logo --*/}
                                        <img className="img-fluid mb-5" alt="Magic Shop Logo" src="/assets/images/user.jpg" /> </div>
                                </div>
                                {/*-- / .row --*/}
                                <div className="row">
                                    <div className="col-12 col-md-6">
                                        <h6 className="text-uppercase text-muted"> Invoiced from </h6>
                                        <h5>Magic Shop</h5>
                                        <ul className="list-unstyled">
                                            <li><span className="text-muted">Address:&nbsp; </span>44 Shirley Ave. West Chicago, IL 60185, USA</li>
                                            <li><span className="text-muted">VAT nr:&nbsp; </span>123456789</li>
                                            <li><span className="text-muted">Chamber of commerce nr:&nbsp; </span>123456789</li>
                                        </ul>
                                        <h6 className="text-uppercase text-muted pt-4"> Invoiced ID </h6>
                                        <p className="mb-4"> #SDF9823KD </p>
                                    </div>
                                    <div className="col-12 col-md-6 text-md-right">
                                        <h6 className="text-uppercase text-muted"> Invoiced to </h6>
                                        <h5>Customer Name</h5>
                                        <ul className="list-unstyled">
                                            <li>44 Shirley Ave. West Chicago</li>
                                            <li>West Chicago, IL 60185</li>
                                            <li> USA</li>
                                        </ul>
                                        <h6 className="text-uppercase text-muted pt-4"> Due date </h6>
                                        <p className="mb-4">
                                            <time datetime="2018-04-23">Apr 23, 2018</time>
                                        </p>
                                    </div>
                                </div>
                                {/*-- / .row --*/}

                                <div className="row">
                                    <div className="col-12">

                                        {/*-- Table --*/}
                                        <table className="table table-bordered table-hover">
                                            <tr>
                                                <th className="text-center" style={{ width: "1%" }}></th>
                                                <th>Product</th>
                                                <th className="text-center" style={{ width: "1%" }}>Qnt</th>
                                                <th className="text-right" style={{ width: "1%" }}>Unit</th>
                                                <th className="text-right" style={{ width: "1%" }}>Amount</th>
                                            </tr>
                                            <tr>
                                                <td className="text-center">1</td>
                                                <td>
                                                    <p className="font-w600 mb-1">Logo Creation</p>
                                                    <div className="text-muted">Logo and business cards design</div>
                                                </td>
                                                <td className="text-center"> 1 </td>
                                                <td className="text-right">$1.800,00</td>
                                                <td className="text-right">$1.800,00</td>
                                            </tr>
                                            <tr>
                                                <td className="text-center">2</td>
                                                <td>
                                                    <p className="font-w600 mb-1">Online Store Design &amp; Development</p>
                                                    <div className="text-muted">Design/Development for all popular modern browsers</div>
                                                </td>
                                                <td className="text-center"> 1 </td>
                                                <td className="text-right">$20.000,00</td>
                                                <td className="text-right">$20.000,00</td>
                                            </tr>
                                            <tr>
                                                <td className="text-center">3</td>
                                                <td>
                                                    <p className="font-w600 mb-1">App Design</p>
                                                    <div className="text-muted">Promotional mobile application</div>
                                                </td>
                                                <td className="text-center"> 1 </td>
                                                <td className="text-right">$3.200,00</td>
                                                <td className="text-right">$3.200,00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" className="font-w600 text-right">Subtotal</td>
                                                <td className="text-right">$25.000,00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" className="font-w600 text-right">Tax (21% included)</td>
                                                <td className="text-right">$5.000,00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" className="font-w600 text-right">Coupon discount (COUPON25)</td>
                                                <td className="text-right"><span className="text-danger"> -$2,000 </span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" className="font-w600 text-right">Shipping</td>
                                                <td className="text-right">$2,000</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" className="font-weight-bold text-uppercase text-right">Total Due</td>
                                                <td className="font-weight-bold text-right">$30.000,00</td>
                                            </tr>
                                        </table>

                                        {/*-- Title --*/}
                                        <h6 className="text-uppercase"> Notes </h6>

                                        {/*-- Text --*/}
                                        <p className="text-muted mb-0"> We really appreciate your business and if there’s anything else we can do, please let us know! Also, should you need us to add VAT or anything else to this order, it’s super easy since this is a template, so just ask! </p>
                                        <div className="text-right"><a className="btn btn-link-primary margin-bottom-none" href="#"><i className="fe fe-printer"></i>&nbsp;Print Invoice</a></div>
                                    </div>
                                </div>
                                {/*-- / .row --*/}
                            </div>
                        </div>
                    </div>


                    {/*-- Modal 1--*/}
                    <div className="modal fade" id="editInvoiceModal" tabIndex="-1" role="dialog" aria-labelledby="editInvoiceModal" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="editInvoiceModalLabel">Edit invoice status and send message to customer</h5>
                                </div>
                                <div className="modal-body">
                                    <form id="changeInvoiceStatus">
                                        <div className="form-group">
                                            <div className="form-label">Change invoice stauts to:</div>
                                            <div className="custom-controls-stacked">
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option1" checked />
                                                    <span className="custom-control-label">Pending</span> </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option2" />
                                                    <span className="custom-control-label">Paid</span> </label>
                                                <label className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" className="custom-control-input" name="example-inline-radios" value="option3" />
                                                    <span className="custom-control-label">Cancled</span> </label>
                                            </div>
                                        </div>
                                        <hr />
                                        <div className="form-group mt-6">
                                            <label className="form-label">Notes</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="Notes" />
                                        </div>
                                    </form>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" className="btn btn-primary">Save changes and send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default EditInvoice