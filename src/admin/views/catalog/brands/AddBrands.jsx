import React, { Fragment, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';


import { CustomInput, Form, CustomTextAreaInput, CustomCheckbox, AlertBox } from '../../../components/Inputs';

const AddBrands = ({ match }) => {
    const parentUrl = ParentUrl(match.path)
    const catalogContext = useContext(CatalogContext)


    const onSubmit = (values, e) => {
        catalogContext.createBrand(values, values.id);
        catalogContext.resetStatus()
        e.target.reset({
            errors: true, // errors will not be reset
            dirtyFields: true, // dirtyFields will not be reset
            dirty: true, // dirty will not be reset
            isSubmitted: false,
            touched: false,
            isValid: false,
            submitCount: false
        });
    }

    useEffect(() => {
        return () => {
            catalogContext.setSuccessMessage(null)
            catalogContext.setErrorMessage(null)
        }
    }, [])

    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    {catalogContext.successMessage && <AlertBox message={catalogContext.successMessage} type="alert-success" />}
                    {catalogContext.errorMessage && <AlertBox message={catalogContext.errorMessage} type="alert-danger" />}

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">
                                Add Brands
                            </h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-align-justify mr-2"></i>View all Brand</Link>

                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Brand information</h3>
                        </div>
                        <div className="card-body">
                            <Form onSubmit={onSubmit} >
                                <div className="row">
                                    <div className="col-md-6">
                                        <CustomInput
                                            label="Brand Name"
                                            placeholder="Enter brand name"
                                            name="name"
                                            type="text"

                                            required
                                            minLength="10"
                                        />
                                        <CustomTextAreaInput
                                            label="Brand short description"
                                            placeholder="Enter barnd short description between 50-160 characters"
                                            name="description"
                                            rows="6"
                                            type="text"
                                            required
                                            maxLength="100"
                                        />
                                        <CustomInput
                                            label="Brand origin"
                                            placeholder="Enter brand origin"
                                            name="origin"
                                            type="text"
                                            required
                                            minLength="2"
                                        />
                                        <CustomCheckbox
                                            name="status"
                                            label="Publish now?"
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <CustomInput
                                            label="SEO meta keywords"
                                            placeholder="keywords, sperated, by, comma"
                                            name="seo_keyword"
                                            type="text"
                                            required
                                            minLength="10"
                                        />
                                        <CustomTextAreaInput
                                            label="SEO meta description"
                                            placeholder="Enter barnd meta description between 50–160 characters."
                                            name="seo_meta_description"
                                            rows="6"
                                            type="text"
                                            maxLength="100"
                                            minLength="50"
                                        />
                                        <CustomInput
                                            label="image"
                                            // placeholder="Enter category name"
                                            name="image"
                                            type="file"
                                            required
                                            minLength="10"
                                        />
                                    </div>

                                </div>
                                <div className="text-right">
                                    <button type="submit" className="btn btn-md btn-primary">Save Changes</button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment >
    )
}

export default AddBrands