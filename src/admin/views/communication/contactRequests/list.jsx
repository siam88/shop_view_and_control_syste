import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = () => {
    const { path } = useRouteMatch();

    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>status</th>
                        <th>action</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>16-06-2019</td>
                        <td>Customer first name</td>
                        <td>last name</td>
                        <td>customer@gmail.com</td>
                        <td>1234567890</td>

                        <td>
                            <span className="badge badge-warning">new</span>
                        </td>
                        <td>
                            <Link to={`${path}/edit`} className="btn btn-sm"><i className="fas fa-edit mr-2"></i></Link>
                            <Link to="" className="btn btn-sm"><i className="fas fa-archive mr-2"></i></Link>

                        </td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default List