import React, { Fragment, useState, useEffect } from 'react'
import { Link } from 'react-router-dom';
import BlogCategoryList from './BlogCategoryList';
import axios from 'axios';

const Category = ({ match }) => {
	const parentUrl = match.path.slice(0, match.path.lastIndexOf("/"));

	const [categories, setCategories] = useState([]);

	useEffect(() => {
		async function fetchCategories() {
			axios.get(`/categories`)
				.then(res => setCategories(res.data))
		}
		fetchCategories();
	}, []);

	return (
		<Fragment>
			<div className="row">
				<div className="col-sm-12">

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header page-title-card p-4">
							<h3 className="card-title">Manage blog categories</h3>
							<div className="card-options">
								<Link to={`${parentUrl}`} className="btn btn-primary"><i className="fas fa-reply"></i></Link>
								<Link to={`${match.path}/add`} className="btn btn-primary ml-2"><i className="fas fa-plus-square mr-2"></i>Add blog category </Link>
								<Link to={`${match.path}/archived`} className="btn btn-outline-info ml-2"><i className="fas fa-archive mr-2"></i>View archived categories</Link>
							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-6">
							<h3 className="card-title">blog category list</h3>
						</div>
						<div className="card-body">
							<BlogCategoryList categories={categories} />
						</div>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default Category