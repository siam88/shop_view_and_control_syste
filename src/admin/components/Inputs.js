import React, { Fragment } from "react"
import { useForm, ErrorMessage } from "react-hook-form";






export function Form({ children, onSubmit, values }) {
    const methods = useForm({ defaultValues: values });
    const { handleSubmit } = methods;


    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            {

                Array.isArray(children)
                    ?
                    children.map((child, index) => {
                        // console.log(child)
                        if (!Array.isArray(child.props.children)) {
                            return child.props.name ? React.createElement(child.type, {
                                ...{
                                    ...child.props,
                                    register: methods.register,
                                    key: child.props.name,
                                    errors: methods.errors
                                }
                            }) : child
                        }
                        else {

                            return child.type == 'div' ? React.createElement('div', {

                                children: child.props.children.map((child1, index2) => {
                                    // console.log('child1',child1);
                                    if (!Array.isArray(child1.props.children)) {
                                        return React.createElement(child1.type, {
                                            ...{
                                                ...child1.props,
                                                register: methods.register,
                                                key: child1.props.name,
                                                errors: methods.errors
                                            }
                                        })
                                    } else {
                                        return child1.type == 'div' ? React.createElement('div', {
                                            children: child1.props.children.map(child2 => {
                                                if (!Array.isArray(child2.props.children)) {
                                                    return React.createElement(child2.type, {
                                                        ...{
                                                            ...child2.props,
                                                            register: methods.register,
                                                            key: child2.props.name,
                                                            errors: methods.errors
                                                        }
                                                    })
                                                }
                                            }),
                                            key: index2,
                                            className: child1.props.className
                                        }) : child1
                                    }
                                }),
                                key: index,
                                className: child.props.className
                            }) : child

                        }

                    })
                    : children

            }

        </form>
    );
}












export const CustomInput = ({ register, label, errors, appendClass, name, required, minLength, maxLength, ...rest }) => {
    // console.log(errors)
    return (
        <Fragment>
            <div className={`form-group ${appendClass ? appendClass : ''} `}>

                <label className="form-label">
                    {label}
                    {<span span="true" className="form-required" style={{ color: "red" }}> * </span>}
                </label>
                <input className="form-control" multiple name={name} ref={register({ required, minLength })} {...rest} />

                {errors[name]?.type === "required" && <span className="text-danger">This {name} field is required</span>}
                {errors[name]?.type === "minLength" && <span className="text-danger">Min length need to fill up</span>}
                {errors[name]?.type === "maxLength" && <span className="text-danger">Max length exceeded</span>}

            </div>
        </Fragment>
    )
}

export const CustomTextAreaInput = ({ register, label, errors, appendClass, rules, name, required, minLength, maxLength, ...rest }) => {
    // const required = rules && rules.required;
    return (
        <Fragment >
            <div className={`form-group ${appendClass} `}>
                <div className="form-label">
                    {label}
                    {required && <span className="form-required" style={{ color: "red" }}> * </span>}
                </div>
                <textarea className="form-control" name={name} ref={register({ required, minLength, maxLength })} {...rest} />

                {errors[name]?.type === "required" && <span className="text-danger">This {name} field is required</span>}
                {errors[name]?.type === "minLength" && <span className="text-danger">Min length need to fill up</span>}
                {errors[name]?.type === "maxLength" && <span className="text-danger">Max length exceeded</span>}

            </div >
        </Fragment>
    )
}

export const CustomCheckbox = ({ register, label, appendClass, name, ...rest }) => {
    return (
        <Fragment >
            <div className={`form-check ${appendClass} `}>
                <div className="custom-control custom-checkbox mb-3">

                    <input type="checkbox" className="custom-control-input" id="customCheck" name={name} />
                    <label className="custom-control-label" htmlFor="customCheck">{label}</label>
                </div>
            </div>
        </Fragment>
    )
}

export const AlertBox = ({ message, type }) => {

    return (
        <Fragment >

            <div className={`alert ${type} alert-dismissible fade show`}>
                <button type="button" className="close" data-dismiss="alert">×</button>
                {message}
            </div>
        </Fragment>
    )
}