import React from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';

const EditPromotionsAndDeals = ({ match }) => {
    const parentUrl = ParentUrl(match.path)
    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="alert alert-success alert-dismissible">
                        <button type="button" className="close" data-dismiss="alert" />
                        Show message after succesful CRUD
                    </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Editing promotion: here come the promotion name
</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-tag mr-2"></i>View all Promotions</Link>

                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Promotion information
</h3>
                        </div>
                        <div className="card-body">
                            <form id="addPromotion">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Promotion name<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="Special offer" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Starts at</label>
                                            <div className="row gutters-xs">

                                                <div className="col-3">
                                                    <select name="user[day]" className="form-control custom-select">
                                                        <option value="">Day</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                        <option value="16">16</option>
                                                        <option value="17">17</option>
                                                        <option value="18">18</option>
                                                        <option value="19">19</option>
                                                        <option selected="selected" value="20">20</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>
                                                        <option value="30">30</option>
                                                        <option value="31">31</option>
                                                    </select>
                                                </div>
                                                <div className="col-5">
                                                    <select name="user[month]" className="form-control custom-select">
                                                        <option value="">Month</option>
                                                        <option value="1">January</option>
                                                        <option value="2">February</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option selected="selected" value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">August</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                </div>
                                                <div className="col-4">
                                                    <select name="user[year]" className="form-control custom-select">
                                                        <option value="">Year</option>
                                                        <option value="2014">2014</option>
                                                        <option value="2013">2013</option>
                                                        <option value="2012">2012</option>
                                                        <option value="2011">2011</option>
                                                        <option value="2010">2010</option>
                                                        <option value="2009">2009</option>
                                                        <option value="2008">2008</option>
                                                        <option value="2007">2007</option>
                                                        <option value="2006">2006</option>
                                                        <option value="2005">2005</option>
                                                        <option value="2004">2004</option>
                                                        <option value="2003">2003</option>
                                                        <option value="2002">2002</option>
                                                        <option value="2001">2001</option>
                                                        <option value="2000">2000</option>
                                                        <option value="1999">1999</option>
                                                        <option value="1998">1998</option>
                                                        <option value="1997">1997</option>
                                                        <option value="1996">1996</option>
                                                        <option value="1995">1995</option>
                                                        <option value="1994">1994</option>
                                                        <option value="1993">1993</option>
                                                        <option value="1992">1992</option>
                                                        <option value="1991">1991</option>
                                                        <option value="1990">1990</option>
                                                        <option selected="selected" value="1989">1989</option>
                                                       >

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Regular price<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="800" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Promotion link<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="url" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Promotion description<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="FLOS Indoor Lighting" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Ends at</label>
                                            <div className="row gutters-xs">

                                                <div className="col-3">
                                                    <select name="user[day]" className="form-control custom-select">
                                                        <option value="">Day</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                        <option value="16">16</option>
                                                        <option value="17">17</option>
                                                        <option value="18">18</option>
                                                        <option value="19">19</option>
                                                        <option selected="selected" value="20">20</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>
                                                        <option value="30">30</option>
                                                        <option value="31">31</option>
                                                    </select>
                                                </div>
                                                <div className="col-5">
                                                    <select name="user[month]" className="form-control custom-select">
                                                        <option value="">Month</option>
                                                        <option value="1">January</option>
                                                        <option value="2">February</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option selected="selected" value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">August</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                </div>
                                                <div className="col-4">
                                                    <select name="user[year]" className="form-control custom-select">
                                                        <option value="">Year</option>
                                                        <option value="2014">2014</option>
                                                        <option value="2013">2013</option>
                                                        <option value="2012">2012</option>
                                                        <option value="2011">2011</option>
                                                        <option value="2010">2010</option>
                                                        <option value="2009">2009</option>
                                                        <option value="2008">2008</option>
                                                        <option value="2007">2007</option>
                                                        <option value="2006">2006</option>
                                                        <option value="2005">2005</option>
                                                        <option value="2004">2004</option>
                                                        <option value="2003">2003</option>
                                                        <option value="2002">2002</option>
                                                        <option value="2001">2001</option>
                                                        <option value="2000">2000</option>
                                                        <option value="1999">1999</option>
                                                        <option value="1998">1998</option>
                                                        <option value="1997">1997</option>
                                                        <option value="1996">1996</option>
                                                        <option value="1995">1995</option>
                                                        <option value="1994">1994</option>
                                                        <option value="1993">1993</option>
                                                        <option value="1992">1992</option>
                                                        <option value="1991">1991</option>
                                                        <option value="1990">1990</option>
                                                        <option selected="selected" value="1989">1989</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Discounted price<span className="form-required">*</span></label>
                                            <input type="text" className="form-control" placeholder="560" />
                                        </div>

                                        <div className="form-group">
                                            <label className="form-label">Current images</label>
                                            <div className="row gutters-sm">
                                                <div className="col-sm-2">
                                                    <label className="imagecheck mb-4">
                                                        <input name="imagecheck" type="checkbox" value="1" className="imagecheck-input" />
                                                        <figure className="imagecheck-figure"> <img src="/assets/images/user.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                    </label>
                                                </div>
                                                <div className="col-sm-2">
                                                    <label className="imagecheck mb-4">
                                                        <input name="imagecheck" type="checkbox" value="2" className="imagecheck-input" checked />
                                                        <figure className="imagecheck-figure"> <img src="/assets/images/user.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                    </label>
                                                </div>
                                                <div className="col-sm-2">
                                                    <label className="imagecheck mb-4">
                                                        <input name="imagecheck" type="checkbox" value="3" className="imagecheck-input" />
                                                        <figure className="imagecheck-figure"> <img src="/assets/images/user.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                    </label>
                                                </div>
                                                <div className="col-sm-2">
                                                    <label className="imagecheck mb-4">
                                                        <input name="imagecheck" type="checkbox" value="4" className="imagecheck-input" checked />
                                                        <figure className="imagecheck-figure"> <img src="/assets/images/user.jpg" alt="}" className="imagecheck-image" /> </figure>
                                                    </label>
                                                </div>

                                            </div>
                                            <div className="text-right">
                                                <button type="submit" className="btn btn-sm btn-danger">Delete slected images</button>
                                            </div>
                                        </div>
                                        <div className="mb-4">
                                            (show image thumbs after upload)
                      <div className="row row-cards row-deck mt-4 mb-4">
                                                <div className="col-sm-6 col-xl-3 p-2"> <img className="rounded" src="/assets/images/user.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
                                                <div className="col-sm-6 col-xl-3 p-2"> <img className="rounded" src="/assets/images/user.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
                                                <div className="col-sm-6 col-xl-3 p-2"> <img className="rounded" src="/assets/images/user.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
                                                <div className="col-sm-6 col-xl-3 p-2"> <img className="rounded" src="/assets/images/user.jpg" alt="And this isn&#39;t my nose. This is a false one." /> </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-label">Add category image(multiple image upload possible)</div>
                                            <div className="custom-file">
                                                <input type="file" className="custom-file-input" name="example-file-input-custom" />
                                                <label className="custom-file-label">Choose images</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-4">

                                    <div className="col-6">

                                        <div className="form-group">
                                            <div className="form-label">Make it active?</div>
                                            <label className="custom-switch">
                                                <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                <span className="custom-switch-indicator"></span> </label>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="text-right">
                                            <button type="submit" className="btn btn-sm btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default EditPromotionsAndDeals