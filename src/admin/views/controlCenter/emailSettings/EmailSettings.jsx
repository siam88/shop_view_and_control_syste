import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';

const EmailSettings = ({ match }) => {
    const parentUrl = ParentUrl(match.path)
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Email</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}/store-information`} className="btn btn-outline-dark ml-2"><i className="fas fa-info mr-2"></i>Store information</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Email settings</h3>
                        </div>
                        <div className="card-body">
                            <form id="localization">
                                <div className="row">
                                    <div className="col-md-6"> </div>
                                    <div className="col-md-6"> </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">E-mail</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="Store email address" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">SMTP Host</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="Server name for outgoing emails" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">SMTP user name</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="user name to login to smtp host" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">SMTP password</label>
                                            <input type="password" className="form-control" value="password" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">SMTP port</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="587" />
                                        </div>

                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Email Encryption</label>
                                            <select className="form-control custom-select">
                                                <option value="">SSL</option>
                                                <option value="">TLS</option>
                                                <option value="">None</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">POP3 Host</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="Server name for incoming emails" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">POP3 user name</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="user name to login to pop3 host" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">POP3 password</label>
                                            <input type="password" className="form-control" value="password" />
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">POP3 port</label>
                                            <input type="text" className="form-control" name="example-text-input" placeholder="587" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-3"> </div>
                                    <div className="col-3"> </div>
                                    <div className="col-6">
                                        <div className="text-right">
                                            <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default EmailSettings;