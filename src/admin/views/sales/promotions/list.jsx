import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = () => {
    const { path } = useRouteMatch();
    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Promotion name</th>
                        <th>From</th>
                        <th>Till</th>
                        <th>Published</th>

                        <th>action</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span className="avatar avatar-lg" style={{ backgroundImage: 'url(/assets/images/user.jpg)' }} /></td>
                        <td>Special offer Flos indoor lighting</td>
                        <td>18-06-2019	</td>
                        <td>20-07-2019	</td>
                        <td><span className="badge badge-success">Published</span></td>


                        <td>
                            <Link to={`${path}/edit`} className="btn btn-sm "><i className="fas fa-edit"></i></Link>
                            <Link to="" className="btn btn-sm "><i className="fas fa-archive"></i></Link>
                            <Link to="" className="btn btn-sm "><i className="fas fa-eye-slash"></i></Link>
                        </td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default List