import React from "react"
import { Link } from "react-router-dom"
import List from "./List"

const Coupons = ({ match }) => {
    console.log('natchsasds', match.path)
    return (
        <>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage Coupons</h3>
                            <div className="card-options">
                                <Link to={`${match.path}/add`} className="btn btn-primary ml-2"><i className="fas fa-plus mr-2"></i>Add Coupons</Link>
                                <Link to={`${match.path}/archived`} className="btn btn-outline-info ml-2"><i className="fas fa-archive mr-2"></i>View Archived Coupons</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Coupons</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>
                </div>
            </div>
        </>)

}

export default Coupons