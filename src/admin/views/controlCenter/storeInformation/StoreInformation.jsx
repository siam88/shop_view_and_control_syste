import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const StoreInformation = ({ match }) => {
	return (
		<Fragment>
			<div className="row">
				<div className="col-sm-12">
					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header page-title-card p-4">
							<h3 className="card-title">Manage store information</h3>
							<div className="card-options">
								<Link to="" className="btn btn-primary ml-2"><i className="fas fa-eye mr-2"></i>View Storefront</Link>
							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-6">
							<h3 className="card-title">Store main settings</h3>
						</div>
						<div className="card-body">
							<ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
								<li className="nav-item"> <a className="nav-link active" id="pills-general-tab" data-toggle="pill" href="#pills-general" role="tab" aria-controls="pills-general" aria-selected="true">General</a> </li>
								<li className="nav-item"> <a className="nav-link" id="pills-logo-tab" data-toggle="pill" href="#pills-logo" role="tab" aria-controls="pills-logo" aria-selected="false">Logo</a> </li>
								<li className="nav-item"> <a className="nav-link" id="pills-hours-tab" data-toggle="pill" href="#pills-hours" role="tab" aria-controls="pills-hours" aria-selected="false">Working days</a> </li>
								<li className="nav-item"> <a className="nav-link" id="pills-socialmedia-tab" data-toggle="pill" href="#pills-socialmedia" role="tab" aria-controls="pills-socialmedia" aria-selected="false">Social media</a> </li>
								<li className="nav-item"> <a className="nav-link" id="pills-maincolors-tab" data-toggle="pill" href="#pills-maincolors" role="tab" aria-controls="pills-maincolors" aria-selected="false">Appearance</a> </li>
								<li className="nav-item"> <a className="nav-link" id="pills-maintenance-tab" data-toggle="pill" href="#pills-maintenance" role="tab" aria-controls="pills-maintenance" aria-selected="false">Maintenance</a> </li>
							</ul>
							<div className="tab-content p-4" id="pills-tabContent">
								<div className="tab-pane fade show active" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">
									<form id="storeGeneral">
										<div className="row">
											<div className="col-md-6"> </div>
											<div className="col-md-6"> </div>
											<div className="col-md-6">
												<div className="form-group">
													<label className="form-label">Store name<span className="form-required">*</span></label>
													<input type="text" className="form-control" placeholder="Magic Shop" />
												</div>
												<div className="form-group">
													<label className="form-label">Store address<span className="form-required">*</span></label>
													<input type="text" className="form-control" placeholder="514 S. Magnolia St. " />
												</div>
												<div className="form-group">
													<label className="form-label">Store city<span className="form-required">*</span></label>
													<input type="text" className="form-control" placeholder="Orlando " />
												</div>
												<div className="form-group">
													<label className="form-label">Store phone</label>
													<input type="text" className="form-control" placeholder="phone number " />
												</div>
											</div>
											<div className="col-md-6">
												<div className="form-group">
													<label className="form-label">Store website</label>
													<input type="text" className="form-control" placeholder="https://www.magicshop.com" />
												</div>
												<div className="form-group">
													<label className="form-label">Store postcode<span className="form-required">*</span></label>
													<input type="text" className="form-control" placeholder="FL 32806" />
												</div>
												<div className="form-group">
													<label className="form-label">Store country<span className="form-required">*</span></label>
													<select name="country" id="select-countries" className="form-control custom-select">
														<option value="br" data-data='{"image": "./assets/images/flags/br.svg"}'>Brazil</option>
														<option value="cz" data-data='{"image": "./assets/images/flags/cz.svg"}'>Czech Republic</option>
														<option value="de" data-data='{"image": "./assets/images/flags/de.svg"}'>Germany</option>
														<option value="pl" data-data='{"image": "./assets/images/flags/pl.svg"}' selected>Poland</option>
													</select>
												</div>
												<div className="form-group">
													<label className="form-label">Store email(will be populated from smtp settings)</label>
													<input type="text" className="form-control" placeholder="info@magicshop.com " disabled />
												</div>
											</div>
										</div>
										<div className="row mt-4">
											<div className="col-3">
												<div className="form-group">
													<div className="form-label">Show store email?</div>
													<label className="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
														<span className="custom-switch-indicator"></span> </label>
												</div>
											</div>
											<div className="col-3">
												<div className="form-group">
													<div className="form-label">Show store phone?</div>
													<label className="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
														<span className="custom-switch-indicator"></span> </label>
												</div>
											</div>
											<div className="col-6">
												<div className="text-right">
													<button type="submit" className="btn btn-lg btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div className="tab-pane fade" id="pills-logo" role="tabpanel" aria-labelledby="pills-logo-tab">
									<form id="storeLogo">
										<div className="row">
											<div className="col-md-6"> </div>
											<div className="col-md-6"> </div>
											<div className="col-md-6">
												<div className="mb-4"><img src="/assets/images/iphone.jpeg" alt="" className="h-8" /></div>
												<div className="form-group">
													<div className="form-label">Store favicon</div>
													<div className="custom-file">
														<input type="file" className="custom-file-input" name="example-file-input-custom" />
														<label className="custom-file-label">Choose file</label>
													</div>
												</div>
												<div className="mb-4"><img src="/assets/images/iphone.jpeg" alt="" className="h-8" /></div>
												<div className="form-group">
													<div className="form-label">Apple touch icon</div>
													<div className="custom-file">
														<input type="file" className="custom-file-input" name="example-file-input-custom" />
														<label className="custom-file-label">Choose file</label>
													</div>
												</div>
												<div className="mb-4"><img src="/assets/images/iphone.jpeg" alt="" className="h-8" /></div>
												<div className="form-group">
													<div className="form-label">Apple touch icon retina (180px 180px)</div>
													<div className="custom-file">
														<input type="file" className="custom-file-input" name="example-file-input-custom" />
														<label className="custom-file-label">Choose file</label>
													</div>
												</div>
											</div>
											<div className="col-md-6">
												<div className="mb-4"><img src="/assets/images/iphone.jpeg" alt="" className="h-8" /></div>
												<div className="form-group">
													<div className="form-label">Store logo</div>
													<div className="custom-file">
														<input type="file" className="custom-file-input" name="example-file-input-custom" />
														<label className="custom-file-label">Choose file</label>
													</div>
												</div>
												<div className="mb-4"><img src="/assets/images/iphone.jpeg" alt="" className="h-8" /></div>
												<div className="form-group">
													<div className="form-label">Apple touch icon ipad (152px-152px)</div>
													<div className="custom-file">
														<input type="file" className="custom-file-input" name="example-file-input-custom" />
														<label className="custom-file-label">Choose file</label>
													</div>
												</div>
												<div className="mb-4"><img src="/assets/images/iphone.jpeg" alt="" className="h-8" /></div>
												<div className="form-group">
													<div className="form-label">Apple touch icon ipad retina (167px-167px)</div>
													<div className="custom-file">
														<input type="file" className="custom-file-input" name="example-file-input-custom" />
														<label className="custom-file-label">Choose file</label>
													</div>
												</div>
											</div>
										</div>
										<div className="row mt-4">
											<div className="col-3"> </div>
											<div className="col-3"> </div>
											<div className="col-6">
												<div className="text-right">
													<button type="submit" className="btn btn-lg btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div className="tab-pane fade" id="pills-hours" role="tabpanel" aria-labelledby="pills-hours-tab">
									<form id="storeHours">
										<div className="row">
											<div className="col-md-6"> </div>
											<div className="col-md-6"> </div>
											<div className="col-md-6">
												<div className="form-group">
													<label className="form-label">Monday-Friday from</label>
													<input type="text" name="field-name" className="form-control" data-mask="00:00:00" data-mask-clearifnotmatch="true" placeholder="00:00:00" />
												</div>
												<div className="form-group">
													<label className="form-label">Saturday-Sunday from</label>
													<input type="text" name="field-name" className="form-control" data-mask="00:00:00" data-mask-clearifnotmatch="true" placeholder="00:00:00" />
												</div>
											</div>
											<div className="col-md-6">
												<div className="form-group">
													<label className="form-label">Till</label>
													<input type="text" name="field-name" className="form-control" data-mask="00:00:00" data-mask-clearifnotmatch="true" placeholder="00:00:00" />
												</div>
												<div className="form-group">
													<label className="form-label">Till</label>
													<input type="text" name="field-name" className="form-control" data-mask="00:00:00" data-mask-clearifnotmatch="true" placeholder="00:00:00" />
												</div>
											</div>
										</div>
										<div className="row mt-4">
											<div className="col-3">
												<div className="form-group">
													<div className="form-label">Show working hours in the footer?</div>
													<label className="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
														<span className="custom-switch-indicator" ></span> </label>
												</div>
											</div>
											<div className="col-3">
												<div className="form-group">
													<div className="form-label">Show working hours box in contact page?</div>
													<label className="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
														<span className="custom-switch-indicator"></span> </label>
												</div>
											</div>
											<div className="col-6">
												<div className="text-right">
													<button type="submit" className="btn btn-lg btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div className="tab-pane fade" id="pills-socialmedia" role="tabpanel" aria-labelledby="pills-socialmedia-tab">
									<form id="storeSocialMedia">
										<div className="row">
											<div className="col-md-6"> </div>
											<div className="col-md-6"> </div>
											<div className="col-md-6">
												<div className="form-group">
													<label className="form-label">Facebook link:</label>
													<input type="text" className="form-control" name="example-text-input" placeholder="Text.." />
												</div>
												<div className="form-group">
													<label className="form-label">YouTube link:</label>
													<input type="text" className="form-control" name="example-text-input" placeholder="Text.." />
												</div>
											</div>
											<div className="col-md-6">
												<div className="form-group">
													<label className="form-label">Twitter link:</label>
													<input type="text" className="form-control" name="example-text-input" placeholder="Text.." />
												</div>
												<div className="form-group">
													<label className="form-label">LinkedIn link:</label>
													<input type="text" className="form-control" name="example-text-input" placeholder="Text.." />
												</div>
											</div>
										</div>
										<div className="row mt-4">
											<div className="col-3"> </div>
											<div className="col-3"> </div>
											<div className="col-6">
												<div className="text-right">
													<button type="submit" className="btn btn-lg btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div className="tab-pane fade" id="pills-maincolors" role="tabpanel" aria-labelledby="pills-maincolors-tab">
									<form id="maincolors">
										<div className="row">
											<div className="col-md-6">
												<div className="form-group">
													<div className="d-flex align-items-center mb-4">
														<div style={{ backgroundColor: "#377DFF" }} className="w-7 h-7 bg-primary rounded mr-4"></div>
														<div> <strong>Current store color code</strong><br />
															<code>#377DFF</code> </div>
													</div>
													<label className="form-label">New primary store color code:</label>
													<input type="text" className="form-control" name="example-text-input" placeholder="#377DFF" />
												</div>
											</div>
											<div className="col-md-6">
												<div className="d-flex align-items-center mb-4">
													<div style={{ backgroundColor: "#2D1582" }} className="w-7 h-7 rounded mr-4"></div>
													<div> <strong>Current footer color code</strong><br />
														<code>#2D1582</code> </div>
												</div>
												<div className="form-group">
													<label className="form-label">New footer background color code:</label>
													<input type="text" className="form-control" name="example-text-input" placeholder="#2D1582" />
												</div>
											</div>
										</div>
										<div className="row">
											<div className=" col-12">

												<div className="card">
													<img className="card-img-top" src="/assets/images/banner.jpg" alt="footer background" />

												</div>
												<div className="form-group">
													<div className="form-label">Footer background image</div>
													<div className="custom-file">
														<input type="file" className="custom-file-input" name="example-file-input-custom" />
														<label className="custom-file-label">Choose file</label>
													</div>
												</div>
											</div>
										</div>
										<div className="row mt-4">
											<div className="col-3">
												<div className="form-group">
													<div className="form-label">Show currency?</div>
													<label className="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
														<span className="custom-switch-indicator"></span> </label>
												</div>
											</div>
											<div className="col-3">
												<div className="form-group">
													<div className="form-label">Show language?</div>
													<label className="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
														<span className="custom-switch-indicator"></span> </label>
												</div>
											</div>
											<div className="col-6">
												<div className="text-right">
													<button type="submit" className="btn btn-sm btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div className="tab-pane fade" id="pills-maintenance" role="tabpanel" aria-labelledby="pills-maintenance-tab">
									<form id="maintenance">
										<div className="row">
											<div className="col-md-6">
												<div className="form-group">
													<div className="form-label">Turn on the maintenance mode? </div>
													<label className="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" />
														<span className="custom-switch-indicator"></span> </label>
												</div>
											</div>
											<div className="col-md-6">
												<div className="form-group">
													<label className="form-label">Set up Date & Hour</label>
													<input type="text" name="field-name" className="form-control" data-mask="00/00/0000 00:00:00" data-mask-clearifnotmatch="true" placeholder="00/00/0000 00:00:00" />
												</div>
											</div>
										</div>
										<div className="row">
											<div className=" col-12">
												<div className="form-group">
													<label className="form-label">Coming soon header</label>
													<input type="text" className="form-control" name="example-text-input" placeholder="Coming Soon..." />
												</div>
												<div className="form-group">
													<label className="form-label">Coming soon text</label>
													<textarea className="form-control" name="example-textarea-input" rows="6" placeholder="Our website is currently under construction. It goes live in:"></textarea>
												</div>
											</div>
										</div>
										<div className="row mt-4">
											<div className="col-3">
												<div className="form-group">
													<div className="form-label">Show notify me form?</div>
													<label className="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
														<span className="custom-switch-indicator"></span> </label>
												</div>
											</div>
											<div className="col-3"> </div>
											<div className="col-6">
												<div className="text-right">
													<button type="submit" className="btn btn-lg btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default StoreInformation