import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers'

const ViewReview = ({ match }) => {
    const parentUrl = ParentUrl(match.path)

    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="alert alert-success alert-dismissible">
                        <button type="button" className="close" data-dismiss="alert"></button>
            Show message after succesful CRUD
          </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Approve/disapprove customer reveiw</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-thumbs-up"></i> View all reviews</Link>

                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Brand list</h3>
                        </div>
                        <div className="card-body">
                            <div className="col-12">
                                <div className="card card-aside">
                                    <a href="#" className="card-aside-column" style={{ backgroundImage: "url(" + '/assets/images/demo.jpg' + ")" }}></a>
                                    <div className="card-body d-flex flex-column">
                                        <h4><a href="#">Product name</a></h4>
                                        <div className="text-muted">Review text goes here. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
                                        <div className="d-flex align-items-center pt-5 mt-auto">

                                            <div>
                                                <a href="javascript:void(0)" className="text-default">Customer name</a>
                                                <small className="d-block text-muted">Date and time stamp</small>
                                            </div>
                                            <div className="ml-auto text-muted">
                                                <div className="icon d-none d-md-inline-block ml-3"><i className="fa fa-star text-success mr-1"></i><i className="fa fa-star text-success mr-1"></i><i className="fa fa-star text-success mr-1"></i></div>
                                            </div>
                                        </div>
                                        <div className="card-options pt-2">
                                            <a href="#" className="btn btn-success btn-md">Approve</a>
                                            <a href="#" className="btn btn-danger btn-md ml-2">Disapprove</a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default ViewReview