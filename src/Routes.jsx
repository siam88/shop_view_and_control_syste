import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom';
import Main from './Main';
import Index from './admin/Index'

import ControlPannelLogin from './admin/views/Login/ControlPannelLogin'

export default class Routes extends Component {
   render() {
      return (
         <>
            <Switch>
               <Route path="/shop" component={Main} />
               <Route exact path="/shop-admin/login" component={ControlPannelLogin} />
               <Route path="/shop-admin" component={Index} />

               <Redirect from="/" to="/shop" />

            </Switch>
         </>
      )
   }
}
