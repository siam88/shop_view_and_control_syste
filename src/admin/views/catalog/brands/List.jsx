import React, { useContext, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';

const List = ({ archived }) => {
    const catelogContext = useContext(CatalogContext)
    const { path } = useRouteMatch();


    async function getBrands() {

        const status = await catelogContext.getBrands(archived ? archived : null);
        if (!status) console.log("error getting datas");
    }



    const handleDelete = (brand) => {
        if (archived) {
            catelogContext.deleteBrand(brand, "archived");
        }
        catelogContext.deleteBrand(brand);

    }
    useEffect(() => {
        getBrands();
    }, [])

    return (
        <div className="container">

            <div className="table-responsive">
                <table className="table">
                    <thead>
                        <tr>
                            <th>IMG</th>
                            <th>BRAND NAME</th>
                            <th>BRAND DESC</th>
                            <th>ORIGIN COUNTRY</th>
                            <th>PRODUCT COUNT</th>

                            <th>Published</th>

                            <th>actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        {catelogContext.brands?.map((e, i) =>
                            <tr key={i}>
                                <td><span className="avatar avatar-lg" style={{ backgroundImage: 'url(/assets/images/xyz.jpg)' }} /></td>
                                <td>{e.name}</td>
                                <td>{e.description}</td>
                                <td>{e.origin}</td>

                                <td>100</td>
                                <td>
                                    {
                                        toString(e.status) ? <span className="badge badge-success">published</span> : <span className="badge badge-danger">unpublished</span>
                                    }
                                </td>
                                <td>
                                    <Link
                                        to={archived ? {
                                            pathname: `${path}/edit`,
                                            state: { brand: e, type: "archived" }
                                        } : {
                                                pathname: `${path}/edit`,
                                                state: { brand: e, }
                                            }}
                                        className="btn btn-md ">
                                        <i className="far fa-edit" />
                                    </Link>
                                    <Link to="#" className="btn btn-md " onClick={() => handleDelete(e)} ><i className={archived ? "far fa-trash-alt" : "far fa-hdd"} /></Link>
                                    <Link to="#" className="btn btn-md "  ><i className={archived ? "fas fa-redo" : "far fa-eye-slash"} /></Link>
                                </td>
                            </tr>


                        )}

                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default List












