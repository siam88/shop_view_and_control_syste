import React, { useContext } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, } from 'react-router-dom'
import DefaultLayout from './frontend/layouts/DefaultLayout';
import Home from './Home';
import { AuthContext } from './contexts/AuthContextProvider';
import axios from 'axios';
import { API_URL, ADMIN_API_URL } from './Config';

function App() {
	const { authUser } = useContext(AuthContext);
	(function () {
		if (authUser.authToken) {
			if (authUser.guard == 'admin') {
				axios.defaults.baseURL = ADMIN_API_URL;
			}
			else {
				axios.defaults.baseURL = API_URL;
			}
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + authUser.authToken;
		} else {
			axios.defaults.headers.common['Authorization'] = null;
		}
	})();

	return (
		<div className="App">

			<Router>
				<Route path="/" component={Home} />
			</Router>

		</div>
	);
}

export default App;
