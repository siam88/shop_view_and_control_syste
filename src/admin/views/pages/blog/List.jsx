import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = () => {
	const { path } = useRouteMatch();
	return (
		<Fragment>
			<table className="table">
				<thead>
					<tr>
						<th>IMG</th>
						<th>Article Name</th>
						<th>Article Category</th>
						<th>Added Date</th>
						<th>Published</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><span className="avatar avatar-lg" style={{ backgroundImage: "url(/assets/images/xyz.jpg)" }} />
						</td>
						<td>Article name goes here 1</td>
						<td>Category name here</td>
						<td>01-06-2019</td>

						<td>
							<span className="badge badge-success">published</span>
						</td>
						<td>
							<Link to={`${path}/edit`} className="btn btn-sm "><i className="fas fa-edit"></i></Link>
							<Link to="" className="btn btn-sm "><i className="fas fa-archive"></i></Link>
							<Link to="" className="btn btn-sm "><i className="fas fa-eye-slash"></i></Link>

						</td>
					</tr>
				</tbody>
			</table>
		</Fragment>
	)
}

export default List