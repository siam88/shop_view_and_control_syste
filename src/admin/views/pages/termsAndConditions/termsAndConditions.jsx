import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers'

const ReturnPolicy = ({ match }) => {
    const parentUrl = ParentUrl(match.path)
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"></button>
            Show message after succesful CRUD </div>
                    <div className="card">

                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Page terms and conditions</h3>

                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Terms and conditions</h3>
                        </div>
                        <div className="card-body">
                            <form id="pageAboutus">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="form-label">System page name(not allowed to change due to automatic links)</label>
                                            <input type="text" disabled="" class="form-control" placeholder="Terms and conditions" value="Terms and conditions" />
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Custom page name (SEO Slug) <span class="form-required">*</span></label>
                                            <input type="text" class="form-control" placeholder="Terms and conditions" value="" />
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">SEO meta keywords <span class="form-required">*</span></label>
                                            <input type="text" class="form-control" placeholder="keywords, sperated, by, comma" />
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">SEO meta description <span class="form-required">*</span></label>
                                            <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Enter product meta description between 50–160 characters. "></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label">Page header <span class="form-required">*</span></label>
                                            <input type="text" class="form-control" placeholder="About us page header" value="Smartphones in Every Day Life" />
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Page content<span class="form-required">*</span></label>
                                            <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="This needs to be replaced by Summernote"></textarea>
                                        </div>
                                    </div>

                                </div>



                                <div class="row mt-4">
                                    <div class="col-6"> </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-lg btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default ReturnPolicy