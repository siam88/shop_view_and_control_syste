import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = () => {
	const { path } = useRouteMatch();
	return (
		<Fragment>
			<table className="table">
				<thead>
					<tr>
						<th>IMG</th>
						<th>Product</th>
						<th>category</th>
						<th>brand</th>
						<th>price</th>
						<th>stock</th>
						<th>reviewed</th>
						<th>published</th>
						<th>featured</th>
						<th>actions</th>

					</tr>
				</thead>
				<tbody>
					<tr>
						<td><span className="avatar avatar-lg" style={{ backgroundImage: 'url(/assets/images/xyz.jpg)' }} /></td>
						<td>Product name goes here 1</td>
						<td>Category 1</td>
						<td>Brand 1</td>
						<td>100</td>
						<td>43	</td>
						<td>16 times</td>

						<td>
							<span className="badge badge-success">published</span>
						</td>
						<td>
							<span className="badge badge-danger">not featured</span>
						</td>
						<td>
							<Link to={`${path}/edit`} className="btn btn-md "><i className="fas fa-edit" /></Link>
							<Link tp="" className="btn btn-md "><i className="fas fa-archive" /></Link>
							<Link to="" className="btn btn-md "><i className="fas fa-eye-slash" /></Link>
						</td>
					</tr>
				</tbody>
			</table>
		</Fragment>
	)
}

export default List