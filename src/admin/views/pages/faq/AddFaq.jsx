import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers'

const AddFaq = ({ match }) => {
	const parentUrl = ParentUrl(match.path);
	return (
		<Fragment>
			<div className="row">
				<div className="col-sm-12">
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert"></button>
            Show message after succesful CRUD </div>
					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header page-title-card p-4">
							<h3 className="card-title">Add faq item</h3>
							<div className="card-options">
								<Link to={`${parentUrl}`} className="btn btn-primary"><i class="far fa-question-circle mr-2"></i>View all FAQ items</Link>

							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-6">
							<h3 className="card-title">FAQ item information</h3>
						</div>
						<div className="card-body">
							<form id="addFaqItem">
								<div class="row">
									<div class="col-12">
										<div class="form-group">
											<label class="form-label">FAQ category</label>
											<select class="form-control custom-select">
												<option value="">Category 1</option>
												<option value="">Category 2</option>
											</select>
										</div>
										<div class="form-group">
											<label class="form-label">Question<span class="form-required">*</span></label>
											<input type="text" class="form-control" placeholder="Question" />
										</div>
										<div class="form-group">
											<label class="form-label">Answer<span class="form-required">*</span></label>
											<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Please replace it with summernote"></textarea>
										</div>
									</div>

								</div>
								<div class="row mt-4">

									<div class="col-6">

										<div class="form-group">
											<div class="form-label">Make it active?</div>
											<label class="custom-switch">
												<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" value="1" checked />
												<span class="custom-switch-indicator"></span> </label>
										</div>
									</div>
									<div class="col-6">
										<div class="text-right">
											<button type="submit" class="btn btn-lg btn-primary">Add category</button>
										</div>
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default AddFaq