import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';
import List from './List';
import { parentUrl, ParentUrl } from '../../../components/CommonHelpers';

const Category = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Manage FAQ items</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary"><i className="fas fa-reply"></i> View all FAQ items</Link>

                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Archived FAQ item list</h3>
                        </div>
                        <div className="card-body">
                            <List />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Category