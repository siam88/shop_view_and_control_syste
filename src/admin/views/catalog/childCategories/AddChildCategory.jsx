import React, { Fragment, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';
import { useForm, ErrorMessage } from "react-hook-form";

import { CustomInput, Form, CustomTextAreaInput, CustomCheckbox, AlertBox } from '../../../components/Inputs';

const AddChildCategories = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    const catalogContext = useContext(CatalogContext);


    const onSubmit = (values, e) => {
        catalogContext.createChildCategory(values);
        catalogContext.resetStatus();
        e.target.reset({
            errors: true, // errors will not be reset
            dirtyFields: true, // dirtyFields will not be reset
            dirty: true, // dirty will not be reset
            isSubmitted: false,
            touched: false,
            isValid: false,
            submitCount: false
        });
    }

    useEffect(() => {
        return () => {
            catalogContext.setSuccessMessage(null)
            catalogContext.setErrorMessage(null)
        }
    }, [])
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    {catalogContext.successMessage && <AlertBox message={catalogContext.successMessage} type="alert-success" />}
                    {catalogContext.errorMessage && <AlertBox message={catalogContext.errorMessage} type="alert-danger" />}

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">
                                Add child category
                            </h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-align-justify mr-2"></i>View all categories</Link>

                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Child category information</h3>
                        </div>
                        <div className="card-body">
                            <Form onSubmit={onSubmit} >
                                <div className="row">
                                    <CustomInput
                                        label="Choose a parent category"
                                        placeholder="Enter category number"
                                        name="category_id"
                                        type="text"
                                        required
                                        minLength="1"
                                        appendClass="col-md-12"
                                    />
                                    <div className="col-md-6">

                                        <CustomInput
                                            label="Child category name"
                                            placeholder="Enter category name"
                                            name="name"
                                            type="text"
                                            required
                                            minLength="10"
                                        />
                                        <CustomTextAreaInput
                                            label="Child category short description"
                                            placeholder="Enter category short description between 50-160 characters"
                                            name="description"
                                            rows="6"
                                            type="text"
                                            required
                                            maxLength="100"
                                            minLength="50"
                                        />

                                        <CustomCheckbox
                                            name="status"
                                            label="Make it active?"
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <CustomInput
                                            label="SEO meta keywords"
                                            placeholder="keywords, sperated, by, comma"
                                            name="seo_keyword"
                                            type="text"
                                            required
                                            minLength="10"
                                        />
                                        <CustomTextAreaInput
                                            label="SEO meta description"
                                            placeholder="Enter barnd meta description between 50–160 characters."
                                            name="seo_meta_description"
                                            rows="6"
                                            type="text"
                                            maxLength="100"
                                            minLength="50"
                                        />
                                    </div>

                                </div>
                                <div className="text-right">
                                    <button type="submit" className="btn btn-md btn-primary">Add Child category</button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    )
}

export default AddChildCategories;