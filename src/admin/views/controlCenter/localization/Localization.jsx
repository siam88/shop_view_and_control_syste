import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../views/../components/CommonHelpers'


const Localization = ({ match }) => {

    const parentUrl = ParentUrl(match.path);

    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Localization</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}/store-information`} className="btn btn-outline-dark ml-2"><i className="fas fa-info mr-2"></i>Store Information</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Localization settings</h3>
                        </div>
                        <div className="card-body">
                            <form id="localization">
                                <div className="row">
                                    <div className="col-md-6"> </div>
                                    <div className="col-md-6"> </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Date format</label>
                                            <select className="form-control custom-select">
                                                <option value="">d-m-Y</option>
                                                <option value="">Y-m-d</option>
                                                <option value="">etc</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Default timezone</label>
                                            <select className="form-control custom-select">
                                                <option value="">Europe/Amsterdam</option>
                                                <option value="">America/Adak</option>
                                                <option value="">etc</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="form-label">Time format</label>
                                            <select className="form-control custom-select">
                                                <option value="">24 hours</option>
                                                <option value="">12 hours</option>
                                                <option value="">etc</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Default language and currency</label>
                                            <div className="row gutters-xs">
                                                <div className="col-6">
                                                    <select name="default[language]" className="form-control custom-select">
                                                        <option value="">Dutch</option>
                                                        <option value="1">English</option>

                                                    </select>
                                                </div>

                                                <div className="col-6">
                                                    <select name="default[currency]" className="form-control custom-select">
                                                        <option value="">Euro</option>
                                                        <option value="1">US dollar</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-3"> </div>
                                    <div className="col-3"> </div>
                                    <div className="col-6">
                                        <div className="text-right">
                                            <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default Localization