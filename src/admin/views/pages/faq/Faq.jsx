import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import Table from './List';

const Faq = ({ match }) => {
	return (
		<Fragment>
			<div className="row">
				<div className="col-sm-12">

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header page-title-card p-4">
							<h3 className="card-title">Manage FAQ</h3>
							<div className="card-options">
								<Link to={`${match.path}/add`} className="btn btn-primary"><i className="fas fa-plus-square mr-2"></i>Add item</Link>
								<Link to={`${match.path}/category`} className="btn btn-info ml-2"><i className="fas fa-align-left mr-2"></i>Categories</Link>
								<Link to={`${match.path}/archived`} className="btn btn-outline-info ml-2"><i className="fas fa-archive mr-2"></i>View archived items</Link>
								<button type="button" className="btn btn-outline-dark ml-2" data-toggle="modal" data-target="#pageSettingsModal"><i className="fas fa-cogs mr-2"></i>Page settings</button>
							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-6">
							<h3 className="card-title">FAQ Items</h3>
						</div>
						<div className="card-body">
							<Table />
						</div>
					</div>
				</div>
			</div>



			{/**
			 * pageSettingsModal
			 */}
			<form id="pageSettings">
				<div class="modal fade" id="pageSettingsModal" tabindex="-1" role="dialog" aria-labelledby="pageSettingsModal" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="pageSettingsModalLabel">FAQ page settings</h5>
							</div>
							<div class="modal-body">
								<div class="form-group mt-6">
									<label class="form-label">Page name<span class="form-required">*</span></label>
									<input type="text" class="form-control" name="example-text-input" placeholder="FAQ" />
								</div>
								<div class="form-group mt-6">
									<label class="form-label">Page header</label>
									<input type="text" class="form-control" name="example-text-input" placeholder="FAQ page header" />
								</div>
								<div class="form-group">
									<label class="form-label">Page content</label>
									<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Enter short content above fax boxes"></textarea>
								</div>
								<div class="form-group mt-6">
									<label class="form-label">SEO keywords<span class="form-required">*</span></label>
									<input type="text" class="form-control" name="example-text-input" placeholder="keywords, seperated, by, comma" />
								</div>
								<div class="form-group">
									<label class="form-label">SEO meta description<span class="form-required">*</span></label>
									<textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Enter page meta description between 50–160 characters."></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary">Save changes</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</Fragment>
	)
}

export default Faq