import React from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';

const CustomerAccount = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Customer account</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary ml-2"><i className="fas fa-users mr-2"></i>View all customers</Link>
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-lg-4">
                            <div className="card">
                                <div className="card-status bg-primary"></div>
                                <div className="card-body">
                                    <div className="media"> <span className="avatar avatar-xl mr-5" style={{ backgroundImage: "url(/assets/images/user.jpg" }}></span>
                                        <div className="media-body pt-3">
                                            <h4 className="m-0">Juan Hernandez</h4>
                                            <p className="text-muted mb-0">Created: 03/24/2018 </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-body p-4 mt-2 text-center">
                                    <div className="h1 m-0">$95</div>
                                    <div className="text-muted mb-4">Total value</div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title">Send mail</h3>
                                    <div className="card-options"> <a href="#" className="card-options-collapse" data-toggle="card-collapse"><i className="fe fe-chevron-up"></i></a> <a href="#" className="card-options-fullscreen" data-toggle="card-fullscreen"><i className="fe fe-maximize"></i></a> </div>
                                </div>
                                <div className="card-body">
                                    <form>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-group">
                                                    <label className="form-label">Email-Address</label>
                                                    <input className="form-control" placeholder="roryamp@dayrep.com " />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">Message</label>
                                            <textarea className="form-control" rows="5">Big belly rude boy, million dollar hustler. Unemployed.</textarea>
                                        </div>
                                        <div className="form-footer">
                                            <button className="btn btn-primary btn-block">Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-8">
                            <div className="card">
                                <div className="card-status bg-blue"></div>
                                <div className="card-header">
                                    <h3 className="card-title">Last login: 03/24/2018</h3>
                                    <div className="card-options"> <a href="#" className="card-options-collapse" data-toggle="card-collapse"><i className="fe fe-chevron-up"></i></a> <a href="#" className="card-options-fullscreen" data-toggle="card-fullscreen"><i className="fe fe-maximize"></i></a> </div>
                                </div>
                                <div className="card-body">
                                    <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                        <li className="nav-item"> <a className="nav-link active" id="pills-general-tab" data-toggle="pill" href="#pills-general" role="tab" aria-controls="pills-general" aria-selected="true">General</a> </li>
                                        <li className="nav-item"> <a className="nav-link" id="pills-orders-tab" data-toggle="pill" href="#pills-orders" role="tab" aria-controls="pills-orders" aria-selected="false">Orders</a> </li>
                                        <li className="nav-item"> <a className="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews" role="tab" aria-controls="pills-reviews" aria-selected="false">Reviews</a> </li>
                                        <li className="nav-item"> <a className="nav-link" id="pills-comments-tab" data-toggle="pill" href="#pills-comments" role="tab" aria-controls="pills-comments" aria-selected="false">Comments</a> </li>
                                        <li className="nav-item"> <a className="nav-link" id="pills-wishlist-tab" data-toggle="pill" href="#pills-wishlist" role="tab" aria-controls="pills-wishlist" aria-selected="false">Wishlist</a> </li>
                                    </ul>
                                    <div className="tab-content p-3" id="pills-tabContent">
                                        <div className="tab-pane fade show active" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">
                                            <div className="media">
                                                <div className="media-body">
                                                    <div className="row mt-4">
                                                        <div className="col-3">
                                                            <div className="h5">Email</div>
                                                        </div>
                                                        <div className="col-9">
                                                            <div className="h5">tony@gmail.com</div>
                                                        </div>
                                                        <div className="col-3">
                                                            <div className="h5">Telephone</div>
                                                        </div>
                                                        <div className="col-9">
                                                            <div className="h5">+1-202-555-0110</div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <h5>Billing address</h5>
                                                    <address>
                                                        <strong>Twitter, Inc.</strong><br />
                                                                1355 Market St, Suite 900<br />
                                                                    San Francisco, CA 94103<br />
                                                    </address>
                                                    <hr />
                                                    <h5>Shipping address</h5>
                                                    <address>
                                                        <strong>Twitter, Inc.</strong><br />
                                                                    1355 Market St, Suite 900<br />
                                                                        San Francisco, CA 94103<br />
                                                    </address>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="pills-orders" role="tabpanel" aria-labelledby="pills-orders-tab">
                                            <div className="table-responsive">
                                                <table className="table card-table table-striped table-vcenter">
                                                    <thead>
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Total</th>
                                                            <th>Status</th>
                                                            <th><i className="fe fe-arrow-down-circle text-azure"></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td className="w-50">30-12-2018</td>
                                                            <td>$1.234,67</td>
                                                            <td><span className="tag tag-success">shipped</span></td>
                                                            <td className="w-1"><a href="./ms_edit_order.html" className="icon"><i className="fe fe-eye" data-toggle="tooltip" title="view order"></i></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">
                                            <div className="table-responsive">
                                                <table className="table card-table table-vcenter">
                                                    <thead>
                                                        <tr>
                                                            <th>Product</th>
                                                            <th>Stars</th>
                                                            <th>Satus</th>
                                                            <th><i className="fe fe-arrow-down-circle text-azure"></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div className="h5">Product name goes here</div>
                                                                <div className="small text text-cyan"> Review text goes here. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, </div>
                                                                <div className="mt-2"> <small className="text-muted">31-12-2019 16:20</small> </div>
                                                            </td>
                                                            <td>5</td>
                                                            <td><span className="tag tag-success">approved</span></td>
                                                            <td className="w-1"><a href="./ms_edit_product.html" className="icon"><i className="fe fe-eye" data-toggle="tooltip" title="view product"></i></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="pills-comments" role="tabpanel" aria-labelledby="pills-comments-tab">
                                            <div className="table-responsive">
                                                <table className="table card-table table-vcenter">
                                                    <thead>
                                                        <tr>
                                                            <th>Article</th>
                                                            <th>Status</th>
                                                            <th><i className="fe fe-arrow-down-circle text-azure"></i></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div className="h5">Blog article name goes here</div>
                                                                <div className="small text text-cyan"> Comment text goes here. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, </div>
                                                                <div className="mt-2"> <small className="text-muted">31-12-2019 16:20</small> </div>
                                                            </td>
                                                            <td><span className="tag tag-success">approved</span></td>
                                                            <td className="w-1"><a href="./ms_edit_product.html" className="icon"><i className="fe fe-eye" data-toggle="tooltip" title="view article"></i></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="pills-wishlist" role="tabpanel" aria-labelledby="pills-cwishlist-tab">
                                            <div className="table-responsive">
                                                <table className="table card-table table-striped table-vcenter">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2">Product</th>
                                                            <th>Price</th>
                                                            <th>Stock</th>
                                                            <th>Status</th>
                                                            <th><i className="fe fe-arrow-down-circle text-azure"></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td className="w-1"><span className="avatar" style={{ backgroundImage: "url(/assets/images/xyz.jpg)" }}></span></td>
                                                            <td className="w-50">Product name</td>
                                                            <td>$ 132</td>
                                                            <td className="text-nowrap">126</td>
                                                            <td><span className="tag tag-success">published</span></td>
                                                            <td className="w-1"><a href="./ms_edit_product.html" className="icon"><i className="fe fe-eye" data-toggle="tooltip" title="view product"></i></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}
export default CustomerAccount