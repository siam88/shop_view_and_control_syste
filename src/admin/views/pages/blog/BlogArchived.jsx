import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import ListTable from './List';
import { ParentRoot, ParentUrl } from '../../../components/CommonHelpers';

const Blog = ({ match }) => {
	return (
		<Fragment>
			<div className="row">
				<div className="col-sm-12">

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header page-title-card p-4">
							<h3 className="card-title">Manage Blogs</h3>
							<div className="card-options">
						      <Link to={`${ParentUrl(match.path)}/`} class="btn btn-primary"><i class="fas fa-reply"></i></Link>
                  <Link to={`${ParentUrl(match.path)}/add`} class="btn btn-primary ml-2"><i class="fas fa-plus-square mr-2"></i>Add article </Link>
                  <Link to={`${ParentUrl(match.path)}`} class="btn btn-primary ml-2"><i class="fas fa-list mr-2"></i>View all articles</Link>
							</div>
						</div>
					</div>

					<div className="card">
						<div className="card-status bg-primary"></div>
						<div className="card-header p-6">
							<h3 className="card-title">Add Blog</h3>
						</div>
						<div className="card-body">
							<ListTable />
						</div>
					</div>
				</div>
			</div>

			{/*Modal*/}
			<form method="post" action="http://localhost/magic-shop/shop-admin/pages/blog/settings/update" id="pageSettings">
        <input type="hidden" name="_token" defaultValue="Gs94KR7QFU8mHiTJ1HWWsILv4mjEVXxLiwy8Nsmk" />		<div className="modal fade show" id="pageSettingsModal" tabIndex={-1} role="dialog" aria-labelledby="pageSettingsModal" aria-hidden="true">
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="pageSettingsModalLabel">Blog page settings</h5>
              </div>
              <div className="modal-body">
                <div className="form-group mt-6">
                  <label className="form-label">Page name<span className="form-required">*</span></label>
                  <input type="text" name="page_name" defaultValue="Blog" className="form-control " placeholder="Blog" readOnly />
                </div>
                <div className="form-group mt-6">
                  <label className="form-label">Custom page name (SEO Slug) <span className="form-required">*</span></label>
                  <input type="text" name="seo_slug" className="form-control " placeholder="About us" defaultValue="blog-th-u-dyud-yu" />
                </div>
                <div className="form-group mt-6">
                  <label className="form-label">Page header</label>
                  <input type="text" name="page_header" defaultValue className="form-control" placeholder="Blog page header" />
                </div>
                <div className="form-group">
                  <label className="form-label">Page content</label>
                  <textarea className="form-control" name="page_content" rows={6} placeholder="Enter short content" defaultValue={""} />
                </div>
                <div className="form-group mt-6">
                  <label className="form-label">SEO keywords<span className="form-required">*</span></label>
                  <input type="text" className="form-control " name="seo_keyword" defaultValue="f ghj" placeholder="keywords, seperated, by, comma" />
                </div>
                <div className="form-group">
                  <label className="form-label">SEO meta description<span className="form-required">*</span></label>
                  <textarea className="form-control " name="seo_meta_description" rows={6} placeholder="Enter page meta description between 50–160 characters." defaultValue={"vfdfghgjhh"} />
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" className="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>
      </form>
		</Fragment>
	)
}

export default Blog