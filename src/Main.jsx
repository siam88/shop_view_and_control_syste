import React from 'react'
import { Route, useRouteMatch, Redirect, Switch, useHistory } from 'react-router-dom';
import Header from './frontend/layouts/Header';
import HomePage from './frontend/pages/HomePage';
import AboutUs from './frontend/pages/AboutUs';
import ContactUs from './frontend/pages/ContactUs';

const Main = (props) => {
	const { path } = useRouteMatch();
	// const history = useHistory()
	// const pathName = history.location.pathname;
	return (
		<div>
			<Header />
			<main className="py-4">
				<div className="container">

					<Switch>
						<Route exact path={`${path}`} component={HomePage} />
						<Route path={`${path}/about-us`} component={AboutUs} />
						<Route path={`${path}/contact-us`} component={ContactUs} />
						<Redirect from="/" to="/shop" />
					</Switch>

				</div>
			</main>
		</div>
	)
}

export default Main