import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';

const EditBlogCategory = ({ match }) => {
    const parentUrl = match.path.slice(0, match.path.lastIndexOf("/"));
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"></button>
            Show message after succesful CRUD </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Editing blog category: here comes the blog cat name</h3>
                            <div className="card-options">
                                <Link to={`${parentUrl}`} className="btn btn-primary"><i className="fas fa-list"></i> View all blog categories</Link>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header p-6">
                            <h3 className="card-title">Category information</h3>
                        </div>
                        <div className="card-body">
                            <form id="addCategory">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Category name<span class="form-required">*</span></label>
                                            <input type="text" class="form-control" placeholder="Enter category name" />
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Category short description<span class="form-required">*</span></label>
                                            <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Enter category short description"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">SEO meta keywords</label>
                                            <input type="text" class="form-control" placeholder="keywords, sperated, by, comma" />
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">SEO meta description</label>
                                            <textarea class="form-control" name="example-textarea-input" rows="6" placeholder="Enter product meta description between 50–160 characters. "></textarea>
                                        </div>


                                    </div>
                                </div>
                                <div class="row mt-4">

                                    <div class="col-6">

                                        <div class="form-group">
                                            <div class="form-label">Make it active?</div>
                                            <label class="custom-switch">
                                                <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" value="1" checked />
                                                <span class="custom-switch-indicator"></span> </label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-lg btn-primary">Add category</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default EditBlogCategory