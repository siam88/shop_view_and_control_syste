





import React, { useContext, Fragment, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { CatalogContext } from '../../../../contexts/CatalogContextProvider';

const List = () => {
    const catelogContext = useContext(CatalogContext)
    const { path } = useRouteMatch();


    async function getChildategories() {
        const status = await catelogContext.getArchivedChildategories();
        if (!status) console.log("error getting datas");
    }
    const handleDelete = (category) => {
        catelogContext.deleteArchivedChildCategory(category);

    }

    const handleRedo = (category) => {
        catelogContext.redoArchivedChildCategory(category);
    }
    useEffect(() => {
        getChildategories();
    }, [])

    return (
        <Fragment>

            <div className="table-responsive">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Child Category Name</th>
                            <th>Belongs to Category</th>
                            <th>Product Count</th>
                            <th>Published</th>
                            <th>actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        {catelogContext.archivedChildCategories?.map((e, i) =>
                            <tr key={i}>
                                <td>{e.name}</td>
                                <td>{e.category_id}</td>
                                <td>100</td>
                                <td>
                                    {
                                        e.status ? <span className="badge badge-success">published</span> : <span className="badge badge-danger">unpublished</span>
                                    }
                                </td>
                                <td>
                                    <Link to={{
                                        pathname: `${path}/edit`,
                                        state: { childCategory: e }
                                    }} className="btn btn-md "><i className="far fa-edit" /></Link>
                                    <Link to="#" className="btn btn-md " onClick={() => handleDelete(e)} ><i className="far fa-trash-alt" /></Link>
                                    <Link to="#" className="btn btn-md " onClick={() => handleRedo(e)} ><i className="fas fa-redo" /></Link>
                                </td>
                            </tr>


                        )}

                    </tbody>
                </table>
            </div>
        </Fragment>
    )
}

export default List




