import React, { Fragment } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const List = () => {
    const { path } = useRouteMatch();

    // console.log("path", path)
    return (
        <Fragment>
            <table className="table">
                <thead>
                    <tr>
                        <th>Invoice Id</th>
                        <th>Customer Name</th>
                        <th>Customer Email</th>
                        <th>Invoice Total</th>
                        <th>created</th>
                        <th>status</th>
                        <th>action</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>23456</td>
                        <td>Customer name goes here 1</td>
                        <td>customer@gmail.com</td>
                        <td>	765</td>
                        <td>6 hours ago</td>

                        <td>
                            <span className="badge badge-primary">paid</span>
                        </td>
                        <td>
                            <Link to={`${path}/edit`} className="btn btn-md "><i className="fas fa-edit"></i></Link>
                            <Link to={`${path}/edit`} className="btn btn-md "><i className="fas fa-eye"></i></Link>
                        </td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}

export default List