import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { ParentUrl } from '../../../components/CommonHelpers';
import List from './List';

const CurrencySettings = ({ match }) => {
    const parentUrl = ParentUrl(match.path);
    return (
        <Fragment>
            <div className="row">
                <div className="col-sm-12">
                    <div className="alert alert-success alert-dismissible">
                        <button type="button" className="close" data-dismiss="alert" />Show message after succesful CRUD </div>
                    <div className="card">
                        <div className="card-status bg-primary"></div>
                        <div className="card-header page-title-card p-4">
                            <h3 className="card-title">Editing user: User full name goes here</h3>
                            <div className="card-options">

                                <Link to={`${parentUrl}`} className="btn btn-outline-dark ml-2"><i className="fas fa-info mr-2"></i>View all users</Link>
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-lg-4">
                            <div className="card">
                                <div className="card-status bg-primary"></div>
                                <div className="card-body">
                                    <div className="media"> <span className="avatar avatar-xl mr-5" style={{ backgroundImage: "url(/assets/images/xyz.jpg)" }}></span>
                                        <div className="media-body pt-3">
                                            <h4 className="m-0">Juan Hernandez</h4>
                                            <p className="text-muted mb-0">Created: 03/24/2018 </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-body p-4 mt-2 text-center">
                                    <div className="h1 m-0">admin</div>
                                    <div className="text-muted mb-4">Last login: 03/24/2018</div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title">Send password reset link</h3>
                                    <div className="card-options"> <a href="#" className="card-options-collapse" data-toggle="card-collapse"><i className="fe fe-chevron-up"></i></a> <a href="#" className="card-options-fullscreen" data-toggle="card-fullscreen"><i className="fe fe-maximize"></i></a> </div>
                                </div>
                                <div className="card-body pb-8">
                                    <form>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-group">
                                                    <label className="form-label">Email-Address</label>
                                                    <input className="form-control" name="example-disabled-input" placeholder="your-email@domain.com" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-footer">
                                            <button className="btn btn-primary btn-block">Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-8">
                            <div className="card">
                                <div className="card-status bg-blue"></div>
                                <div className="card-header">
                                    <h3 className="card-title">User information</h3>
                                    <div className="card-options"> <a href="#" className="card-options-collapse" data-toggle="card-collapse"><i className="fe fe-chevron-up"></i></a> <a href="#" className="card-options-fullscreen" data-toggle="card-fullscreen"><i className="fe fe-maximize"></i></a> </div>
                                </div>
                                <div className="card-body">
                                    <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                        <li className="nav-item"> <a className="nav-link active" id="pills-general-tab" data-toggle="pill" href="#pills-general" role="tab" aria-controls="pills-general" aria-selected="true">General</a> </li>
                                        <li className="nav-item"> <a className="nav-link" id="pills-credentials-tab" data-toggle="pill" href="#pills-credentials" role="tab" aria-controls="pills-credentials" aria-selected="false">Credentials</a> </li>

                                    </ul>
                                    <div className="tab-content p-3" id="pills-tabContent">
                                        <div className="tab-pane fade show active" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">
                                            <form id="editUserGeneral">
                                                <div className="row">
                                                    <div className="col-sm-6 col-md-6">
                                                        <div className="form-group">
                                                            <label className="form-label">First Name</label>
                                                            <input type="text" className="form-control" placeholder="first name" value="Juan" />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6 col-md-6">
                                                        <div className="form-group">
                                                            <label className="form-label">Last Name</label>
                                                            <input type="text" className="form-control" placeholder="Last Name" value="Hernandez" />
                                                        </div>
                                                    </div>
                                                    <div className="col-12">

                                                        <div className="form-group">
                                                            <label className="form-label">Phone<span className="form-required">*</span></label>
                                                            <input type="text" className="form-control" placeholder="123456789" value="123456789" />
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="form-label">Email-Address</label>
                                                            <input className="form-control" placeholder="your-email@domain.com" />
                                                        </div>
                                                        <div className="form-group">
                                                            <div className="form-label">Customer avatar</div>
                                                            <div className="custom-file">
                                                                <input type="file" className="custom-file-input" name="example-file-input-custom" />
                                                                <label className="custom-file-label">Choose image</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row mt-4">
                                                    <div className="col-6">
                                                        <div className="form-group">
                                                            <div className="form-label">Active</div>
                                                            <label className="custom-switch">
                                                                <input type="checkbox" name="custom-switch-checkbox" className="custom-switch-input" value="1" checked />
                                                                <span className="custom-switch-indicator"></span> </label>
                                                        </div>
                                                    </div>
                                                    <div className="col-6">
                                                        <div className="text-right">
                                                            <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div className="tab-pane fade" id="pills-credentials" role="tabpanel" aria-labelledby="pills-credentials-tab">
                                            <form id="editUserCredentials">
                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className="form-label">Login name</label>
                                                            <input type="text" className="form-control" name="example-text-input" placeholder="login name" value="Company Name" />
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="form-label">Password</label>
                                                            <input type="password" className="form-control" value="password" />
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="form-label">New password</label>
                                                            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="row mt-4">
                                                    <div className="col-6"> </div>
                                                    <div className="col-6">
                                                        <div className="text-right">
                                                            <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div className="tab-pane fade" id="pills-invoice" role="tabpanel" aria-labelledby="pills-invoice-tab">
                                            <form id="editCustomerInvoice">
                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className="form-label">Company</label>
                                                            <input type="text" className="form-control" name="example-text-input" placeholder="Company name" value="Company Name" />
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="form-label">Address</label>
                                                            <input type="text" className="form-control" name="example-text-input" placeholder="Street name" value="Streetname and number" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-6 col-md-4">
                                                        <div className="form-group">
                                                            <label className="form-label">City</label>
                                                            <input type="text" className="form-control" placeholder="City" value="Melbourne" />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6 col-md-3">
                                                        <div className="form-group">
                                                            <label className="form-label">Postal Code</label>
                                                            <input type="number" className="form-control" placeholder="ZIP Code" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-5">
                                                        <div className="form-group">
                                                            <label className="form-label">Country</label>
                                                            <select className="form-control custom-select">
                                                                <option value="">Germany</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row mt-4">
                                                    <div className="col-6"> </div>
                                                    <div className="col-6">
                                                        <div className="text-right">
                                                            <button type="submit" className="btn btn-lg btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>






        </Fragment>
    )
}

export default CurrencySettings